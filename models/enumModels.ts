export enum ActionStatusEnum {
	OPEN = "open",
	SENT = "sent",
	FAILED = "failed",
	ARCHIVED = "archived"
}

export enum AcceptStatusEnum {
	OPEN = "open",
	ACCEPTED = "accepted",
	ACTIVE = "active",
	PENDING = "pending",
	SUCCEED = "succeed",
	FAILED = "failed",
	ARCHIVED = "archived"
}

export enum UserRoleEnum {
	USER = "user",
	ADMIN = "admin"
}

export enum AccountStatusEnum {
	ACTIVE = 'active',
	PENDING = 'pending',
	RESTRICTED = 'restricted',
	ARCHIVED = 'archived',
	ASSIGNED = 'assigned',
}
