import mongoose, { Document, Model } from "mongoose";
import { AcceptStatusEnum, ActionStatusEnum } from "./enumModels";

export interface Lead extends Document {
  avatar: string;
  name: string;
  role: string;
  url: string;
  country: string;
  category: string;
  company: string;
  location: string;
  industry: string;
  note: string;
  tags: string[];
  resource: string;
  meta_data: any;
  assigned_user?: string;
  assigned_account?: string;
  action_status?: ActionStatusEnum;
  accept_status?: AcceptStatusEnum;
  createdAt: Date;
  updatedAt: Date;
}

const LeadSchema = new mongoose.Schema(
  {
    avatar: { type: String },
    name: { type: String, required: true },
    role: { type: String },
    url: { type: String },
    country: { type: String },
    category: { type: String },
    company: { type: String },
    location: { type: String },
    industry: { type: String },
    note: { type: String },
    tags: { type: [String], default: [] },
    resource: { type: String },
    meta_data: { type: mongoose.Schema.Types.Mixed },
    assigned_user: {type: mongoose.Schema.Types.ObjectId, ref: 'User', required: false},
    assigned_account: {type: mongoose.Schema.Types.ObjectId, ref: 'Account', required: false},
    action_status: {
      type: String,
      enum: Object.values(ActionStatusEnum),
      required: false,
      default: "open",
    },
    accept_status: {
      type: String,
      enum: Object.values(AcceptStatusEnum),
      required: false,
      default: "pending",
    },
  },
  { timestamps: true }
);

const LeadModel: Model<Lead> =
  mongoose.models.Lead || mongoose.model<Lead>("Lead", LeadSchema);
export default LeadModel;
