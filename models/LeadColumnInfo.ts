import mongoose, { Document, Model, Schema, Types } from "mongoose";

// Define the LeadColumnInfo sub-document schema
export interface LeadColumnInfo {
  title: string;
  checked: boolean;
}

const LeadColumnInfoSchema = new Schema<LeadColumnInfo>({
  title: { type: String, required: true },
  checked: { type: Boolean, required: true },
});

// Define the parent schema
export interface UserLeadColumnInfo extends Document {
  userId: Types.ObjectId;
  leadColumnInfo: LeadColumnInfo[];
}

const UserLeadColumnInfoSchema = new Schema<UserLeadColumnInfo>(
  {
    userId: { type: Schema.Types.ObjectId, required: true, ref: "User" },
    leadColumnInfo: { type: [LeadColumnInfoSchema], required: true },
  },
  { timestamps: true }
);

// Create the model
const UserLeadColumnInfoModel: Model<UserLeadColumnInfo> =
  mongoose.models.UserLeadColumnInfo ||
  mongoose.model<UserLeadColumnInfo>(
    "UserLeadColumnInfo",
    UserLeadColumnInfoSchema
  );

export default UserLeadColumnInfoModel;
