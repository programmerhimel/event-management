import mongoose, { Document, Model } from "mongoose";
import bcrypt from 'bcrypt';
import { UserRoleEnum } from "./enumModels";

export interface User extends Document {
  name: string;
  email: string;
  password: string;
  role: UserRoleEnum;
  avatar?: string;
  createdAt: Date;
  updatedAt: Date;
}

const UserSchema = new mongoose.Schema(
  {
    name: { type: String },
    email: { type: String, required: true, unique: true },
    password: { type: String, required: true },
    role: { type: String, enum: Object.values(UserRoleEnum), default: "user" },
    avatar: { type: String },
  },
  { timestamps: true }
);

UserSchema.pre('save', async function (next) {
  const user = this as User;
  if (!user.isModified('password')) return next();

  try {
    const salt = await bcrypt.genSalt(10);
    const hash = await bcrypt.hash(user.password, salt);
    user.password = hash;
    next();
  } catch (error) {
    // @ts-ignore
    next(error);
  }
});

const UserModel: Model<User> =
  mongoose.models.User || mongoose.model<User>("User", UserSchema);

export default UserModel;
