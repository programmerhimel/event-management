import React from "react";
import Sidebar from "../Sidebar";
import Header from "../Header";
import { useSession } from "next-auth/react";

function Layout({ children }: { children: React.ReactNode }) {
  const { data: session, status } = useSession();
  if (status === "loading") return null;
  return (
    <div className="flex flex-col">
      <Header />
      <div className="main flex w-full h-auto">
        <Sidebar user={session?.user!} />
        <div className="flex flex-col w-[calc(100%-256px)] h-full">
          <div className="content bg-white w-full h-full overflow-y-auto">
            <div className="flex flex-col w-full h-[calc(100vh-80px)] overflow-hidden relative">
              {children}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Layout;
