import React, { useContext, useEffect, useMemo, useRef, useState } from "react";
import type { SyntheticListenerMap } from "@dnd-kit/core/dist/hooks/utilities";
import { useSortable } from "@dnd-kit/sortable";
import { CSS } from "@dnd-kit/utilities";
import { Table, Avatar, Tooltip, Space, Button } from "antd";
import type { TableColumnsType, TableProps } from "antd";
import Link from "next/link";
import Copybutton from "../CopyButton";
import LeadForm from "../LeadForm";
import { INITIAL_PAGINATION } from "@/pages/leads";
import { ClientContext } from "@/context/ClientContext";
import moment from "moment";
import Loader from "../Loader";

type TableRowSelection<T> = TableProps<T>["rowSelection"];

interface RowContextProps {
  setActivatorNodeRef?: (element: HTMLElement | null) => void;
  listeners?: SyntheticListenerMap;
}

const RowContext = React.createContext<RowContextProps>({});

interface RowProps extends React.HTMLAttributes<HTMLTableRowElement> {
  "data-row-key": string;
}

const Row: React.FC<RowProps> = (props) => {
  const {
    attributes,
    listeners,
    setNodeRef,
    setActivatorNodeRef,
    transform,
    transition,
    isDragging,
  } = useSortable({ id: props["data-row-key"] });

  const style: React.CSSProperties = {
    ...props.style,
    transform: CSS.Translate.toString(transform),
    transition,
    ...(isDragging ? { position: "relative", zIndex: 9999 } : {}),
  };

  const contextValue = useMemo<RowContextProps>(
    () => ({ setActivatorNodeRef, listeners }),
    [setActivatorNodeRef, listeners]
  );

  return (
    <RowContext.Provider value={contextValue}>
      <tr {...props} ref={setNodeRef} style={style} {...attributes} />
    </RowContext.Provider>
  );
};

function LeadTable({
  data,
  updateSelectedRows,
  user,
  fetchData,
  isVisibleModal,
  setIsVisibleModal,
  selectedRowKeys,
  isLoading
}: {
  data: Lead[];
  updateSelectedRows: Function;
  user: { id: string; email: string; role: "user" | "admin"; name: string };
  fetchData: Function;
  isVisibleModal: boolean;
  setIsVisibleModal: Function;
  selectedRowKeys: string[];
  isLoading: boolean;
}) {
  const elementRef = useRef<HTMLDivElement>(null);
  const [elementHeight, setElementHeight] = useState(0);
  const [isModalVisible, setModalVisible] = useState(false);
  const [selectedLead, setSelectedLead] = useState<Lead>();
  const { storedLeadColumns } = useContext(ClientContext);
  const [sortedInfo, setSortedInfo] = useState<any>({});

  const capitalizeFirstLetter = (str: string) => {
    return str.charAt(0).toUpperCase() + str.slice(1);
  };

  const columns: TableColumnsType<Lead> = [
    {
      title: "Avatar",
      dataIndex: "avatar",
      render: (_: any, record: Lead) => (
        <Avatar
          alt={record.avatar}
          size={"large"}
          className="text-lg font-semibold"
        >
          {record.name?.charAt(0).toUpperCase()}
        </Avatar>
      ),
      ellipsis: true,
      width: 80,
    },
    {
      title: "Name",
      className: 'long-column',
      dataIndex: "name",
      key: "name",
      ellipsis: true,
      sorter: (a, b) => {
        if (!a.name || !b.name) return 0;
        return a.name.localeCompare(b.name);
      },
      sortOrder: sortedInfo.columnKey === "name" && sortedInfo.order,
    },
    {
      title: "Role",
      className: 'long-column',
      dataIndex: "role",
      key: "role",
      ellipsis: true,
      sorter: (a, b) => {
        if (!a.role || !b.role) return 0;
        return a.role.localeCompare(b.role);
      },
      sortOrder: sortedInfo.columnKey === "role" && sortedInfo.order,
    },
    {
      title: "URL",
      className: 'long-column',
      dataIndex: "url",
      key: "url",
      ellipsis: true,
      render: (url: string) => <Copybutton url={url} />,
      sorter: (a, b) => {
        if (!a.url || !b.url) return 0;
        return a.url.localeCompare(b.url);
      },
      sortOrder: sortedInfo.columnKey === "url" && sortedInfo.order,
    },
    {
      title: "Country",
      className: 'long-column',
      dataIndex: "country",
      key: "country",
      ellipsis: true,
      sorter: (a, b) => {
        if (!a.country || !b.country) return 0;
        return a.country.localeCompare(b.country);
      },
      sortOrder: sortedInfo.columnKey === "country" && sortedInfo.order,
    },
    {
      title: "Category",
      className: 'long-column',
      dataIndex: "category",
      key: "category",
      ellipsis: true,
      sorter: (a, b) => {
        if (!a.category || !b.category) return 0;
        return a.category.localeCompare(b.category);
      },
      sortOrder: sortedInfo.columnKey === "category" && sortedInfo.order,
    },
    {
      title: "Company",
      className: 'long-column',
      dataIndex: "company",
      key: "company",
      ellipsis: true,
      sorter: (a, b) => {
        if (!a.company || !b.company) return 0;
        return a.company.localeCompare(b.company);
      },
      sortOrder: sortedInfo.columnKey === "company" && sortedInfo.order,
    },
    {
      title: "Location",
      className: 'long-column',
      dataIndex: "location",
      key: "location",
      ellipsis: true,
      sorter: (a, b) => {
        if (!a.location || !b.location) return 0;
        return a.location.localeCompare(b.location);
      },
      sortOrder: sortedInfo.columnKey === "location" && sortedInfo.order,
    },
    {
      title: "Industry",
      className: 'long-column',
      dataIndex: "industry",
      key: "industry",
      ellipsis: true,
      sorter: (a, b) => {
        if (!a.industry || !b.industry) return 0;
        return a.industry.localeCompare(b.industry);
      },
      sortOrder: sortedInfo.columnKey === "industry" && sortedInfo.order,
    },
    {
      title: "Note",
      className: 'long-column',
      dataIndex: "note",
      key: "note",
      ellipsis: true,
      sorter: (a, b) => {
        if (!a.note || !b.note) return 0;
        return a.note.localeCompare(b.note);
      },
      sortOrder: sortedInfo.columnKey === "note" && sortedInfo.order,
    },
    {
      title: "Tags",
      className: 'long-column',
      dataIndex: "tags",
      render: (tags: string[]) => tags.join(", "),
      ellipsis: true,
    },
    {
      title: "Resource",
      className: 'long-column',
      dataIndex: "resource",
      key: "resource",
      ellipsis: true,
      sorter: (a, b) => {
        if (!a.resource || !b.resource) return 0;
        return a.resource.localeCompare(b.resource);
      },
      sortOrder: sortedInfo.columnKey === "resource" && sortedInfo.order,
    },
    {
      title: "Action Status",
      className: 'long-column',
      dataIndex: "action_status",
      key: "action_status",
      ellipsis: false,
      render: (text) => capitalizeFirstLetter(text.toLowerCase()),
      sorter: (a, b) => {
        if (!a.action_status || !b.action_status) return 0;
        return a.action_status.localeCompare(b.action_status);
      },
      sortOrder: sortedInfo.columnKey === "action_status" && sortedInfo.order,
    },
    {
      title: "Accept Status",
      className: 'long-column',
      dataIndex: "accept_status",
      key: "accept_status",
      ellipsis: false,
      render: (text) => capitalizeFirstLetter(text.toLowerCase()),
      sorter: (a, b) => {
        if (!a.accept_status || !b.accept_status) return 0;
        return a.accept_status.localeCompare(b.accept_status);
      },
      sortOrder: sortedInfo.columnKey === "accept_status" && sortedInfo.order,
    },
    {
      title: "Created At",
      className: 'long-column',
      dataIndex: "createdAt",
      key: "createdAt",
      render: (date: Date) => new Date(date).toLocaleString(),
      ellipsis: true,
      sorter: (a, b) => {
        if (!a.createdAt || !b.createdAt) return 0;
        return moment(a.createdAt).unix() - moment(b.createdAt).unix();
      },
      sortOrder: sortedInfo.columnKey === "createdAt" && sortedInfo.order,
    },
    {
      title: "Updated At",
      className: 'long-column',
      dataIndex: "updatedAt",
      key: "updatedAt",
      render: (date: Date) => new Date(date).toLocaleString(),
      ellipsis: true,
      sorter: (a, b) => {
        if (!a.updatedAt || !b.updatedAt) return 0;
        return moment(a.updatedAt).unix() - moment(b.updatedAt).unix();
      },
      sortOrder: sortedInfo.columnKey === "updatedAt" && sortedInfo.order,
    },
  ];

  const filteredColumns = storedLeadColumns?.filter((col: any) => col.checked)?.map((item: any) => {
    const column = columns.find(
      (col) => item.title === (col.title as string).toLowerCase()
    )
    return column
  }).filter((col: any) => col)

  const actionColumn = {
    title: "Action",
    key: "action",
    render: (_: any, record: Lead) => (
      <Space size="middle">
        <Button type="text" shape="circle" onClick={() => onEdit(record)}>
          <i className="fa-solid fa-edit"></i>
        </Button>
      </Space>
    ),
  };

  useEffect(() => {
    const updateHeight = () => {
      if (elementRef.current) {
        const height = elementRef.current.getBoundingClientRect().height;
        setElementHeight(height);
      }
    };

    const resizeObserver = new ResizeObserver(() => {
      updateHeight();
    });

    if (elementRef.current) {
      resizeObserver.observe(elementRef.current);
    }

    updateHeight();

    return () => {
      if (elementRef.current) {
        resizeObserver.unobserve(elementRef.current);
      }
    };
  }, []);

  const openModal = () => {
    setModalVisible(true);
  };

  const closeModal = () => {
    fetchData();
    setModalVisible(false);
  };

  const onEdit = (lead: Lead) => {
    setSelectedLead(lead);
    openModal();
  };

  const onSelectChange = (newSelectedRowKeys: React.Key[]) => {
    updateSelectedRows(newSelectedRowKeys);
  };

  const handleTableChange = (pagination: any, filters: any, sorter: any) => {
    setSortedInfo(sorter);
  };

  const rowSelection: TableRowSelection<Lead> = {
    selectedRowKeys,
    onChange: onSelectChange,
    // selections: [
    //   Table.SELECTION_ALL,
    //   Table.SELECTION_INVERT,
    //   Table.SELECTION_NONE,
    //   {
    //     key: "odd",
    //     text: "Select Odd Row",
    //     onSelect: (changeableRowKeys) => {
    //       let newSelectedRowKeys = [];
    //       newSelectedRowKeys = changeableRowKeys.filter((_, index) => {
    //         if (index % 2 !== 0) {
    //           return false;
    //         }
    //         return true;
    //       });
    //       setSelectedRowKeys(newSelectedRowKeys);
    //     },
    //   },
    //   {
    //     key: "even",
    //     text: "Select Even Row",
    //     onSelect: (changeableRowKeys) => {
    //       let newSelectedRowKeys = [];
    //       newSelectedRowKeys = changeableRowKeys.filter((_, index) => {
    //         if (index % 2 !== 0) {
    //           return true;
    //         }
    //         return false;
    //       });
    //       setSelectedRowKeys(newSelectedRowKeys);
    //     },
    //   },
    // ],
  };

  return (
    <>
      <LeadForm
        isVisible={isModalVisible}
        onClose={closeModal}
        type="edit"
        lead={selectedLead}
        user={user}
      />
      <div
        ref={elementRef}
        style={{ position: "relative", zIndex: 0 }}
        className="flex-1 h-full overflow-hidden"
      >
        {isLoading && <Loader />}
        <Table
          bordered
          rowKey="_id"
          components={{ body: { row: Row } }}
          rowSelection={rowSelection}
          columns={[...filteredColumns, actionColumn]}
          dataSource={data}
          pagination={false}
          locale={{ emptyText: isLoading ? ' ' : 'No Data' }}
          scroll={{ x: true, y: elementHeight ? elementHeight - 65 : 0 }}
          onChange={handleTableChange}
          className="shadow-md rounded-lg"
        />
        <button
          className="absolute top-0 right-0 px-2 py-1 rounded bg-white hover:scale-110"
          type="button"
          onClick={() => setIsVisibleModal(true)}
        >
          <i className="fa-solid fa-gear"></i>
        </button>
      </div>
    </>
  );
}

export default LeadTable;
