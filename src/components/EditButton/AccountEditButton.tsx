import { ClientContext } from "@/context/ClientContext";
import { Tooltip } from "antd";
import React, { useContext } from "react";

function AccountEditButton({ account }: { account: Account }) {
  const { setEditAccountData, setIsEditMode } = useContext(ClientContext);
  return (
    <Tooltip title={"Edit"}>
      <button
        onClick={() => {
          setIsEditMode(true);
          setEditAccountData(account);
        }}
      >
        <i className="fa-solid fa-edit text-base text-gray-600 hover:text-gray-700 hover:scale-110"></i>
      </button>
    </Tooltip>
  );
}

export default AccountEditButton;
