import { ClientContext } from "@/context/ClientContext";
import { Tooltip } from "antd";
import React, { useContext } from "react";

function UserEditButton({ user }: { user: User }) {
  const { setEditUserData, setIsEditMode } = useContext(ClientContext);
  return (
    <Tooltip title={"Edit"}>
      <button
        onClick={() => {
          setIsEditMode(true);
          setEditUserData(user);
        }}
      >
        <i className="fa-solid fa-edit text-base text-gray-600 hover:text-gray-700 hover:scale-110"></i>
      </button>
    </Tooltip>
  );
}

export default UserEditButton;
