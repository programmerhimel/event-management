import { Avatar, Dropdown, MenuProps } from "antd";
import { signOut, useSession } from "next-auth/react";
import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/router";
import React from "react";

function Header() {
  const { data: session } = useSession();
  const router = useRouter();

  if (!session) {
    return <></>;
  }

  if (!session.user) {
    return (
      <p className="mx-5 my-10 px-3 py-2 text-white rounded-md bg-gray-500">
        User information not available.
      </p>
    );
  }

  const { user } = session;

  const handleLogout = async () => {
    await signOut();
    router.push("/signin");
  };
  const items: MenuProps["items"] = [
    {
      label: <button onClick={handleLogout}>Logout</button>,
      key: "0",
    },
  ];
  return (
    <div className="w-full text-white text-sm font-medium bg-[#0e1425] shadow shadow-gray-800 z-[1]">
      <div className="flex justify-between items-center px-5 py-5">
        <Link href={"/leads"}>
          {/* <div className="relative w-[120px] h-10">
            <Image
              src={"/logo.svg"}
              alt={"Logo image"}
              fill
              className="object-contain"
            />
          </div> */}
          <h1 className="text-2xl">Event Management</h1>
        </Link>
        <div className="flex items-center gap-2 font-normal group">
          <Dropdown menu={{ items }}>
            <div className="flex gap-2 items-center cursor-pointer">
              <div className="">
                <Avatar
                  alt={user.name}
                  size={"large"}
                  className="text-lg font-semibold bg-white text-gray-700"
                >
                  {user.name?.charAt(0)?.toUpperCase()}
                </Avatar>
              </div>
              <h3 className="text-base">{user.email}</h3>
            </div>
          </Dropdown>
        </div>
      </div>
    </div>
  );
}

export default Header;
