import React, { useState } from 'react';
import { parse } from 'papaparse';
import { Upload, Button, message, Modal } from 'antd';
import { UploadOutlined } from '@ant-design/icons';
import type { UploadFile } from 'antd/es/upload/interface';

const FileUpload: React.FC = () => {
  const [parsedData, setParsedData] = useState<any[]>([]);
  const [loading, setLoading] = useState(false);
  const [showModal, setShowModal] = useState(false);
  const [error, setError] = useState<string | null>(null);

  const handleFileChange = (fileList: UploadFile[]) => {
    if (fileList.length > 0) {
      const allParsedData: any[] = [];
      let filesProcessed = 0;

      fileList.forEach((file) => {
        const reader = new FileReader();
        reader.onload = (e) => {
          const text = e.target?.result as string;
          parse(text, {
            header: true,
            transformHeader: (header) => header.toLowerCase(),
            complete: (result) => {
              // @ts-ignore
              const filteredData = result.data.filter((row: any) =>
                Object.values(row).some((value) => value !== '')
              );
              allParsedData.push(...filteredData);
              filesProcessed += 1;
              if (filesProcessed === fileList.length) {
                setParsedData(allParsedData);
              }
            }
          });
        };
        reader.readAsText(file.originFileObj as Blob);
      });
    }
  };

  const handleUpload = async () => {
    setLoading(true);
    setError(null);

    try {
      const response = await fetch('/api/bulkLeads', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(parsedData)
      });

      const data = await response.json();
      if (!response.ok) {
        setError(data?.error || 'Error Uploading Leads');
        return;
      }
      message.success(`${data.added || 0} leads imported successfully. ${data.duplicates && data.duplicates > 0 ? `There are ${data.duplicates} duplicates.` : ''}`);
      setShowModal(false);
    } catch (error) {
      console.error('Error uploading files:', error);
      setError('Error uploading files');
    } finally {
      setLoading(false);
    }
  };

  return (
    <div>
      <button
        className="flex gap-2 items-center bg-[#0070f3] hover:bg-[#4681f4] rounded-lg px-3 py-2 text-base font-normal text-white"
        onClick={() => setShowModal(true)}
      >
        Import CSV files
      </button>

      <Modal
        title="Import CSV files"
        open={showModal}
        onCancel={() => {
          setError(null);
          setShowModal(false);
        }}
        onOk={handleUpload}
        centered
        footer={[
          <div key="submit" className="w-full">
            {error && (
              <div className="m-3">
                <p className="text-red-600">{error}</p>
              </div>
            )}
            <div className="my-5 flex justify-end items-center gap-3">
              <button
                className="flex w-full max-w-[120px] justify-center items-center gap-1 rounded-lg bg-indigo-600 px-3 py-1.5 text-sm font-semibold leading-6 text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
                onClick={handleUpload}
                disabled={parsedData.length === 0 || loading}
              >
                {loading ? 'Importing...' : 'Import'}
              </button>
              {loading && <div className="spinner"></div>}
            </div>
          </div>
        ]}
      >
        <div className="my-5">
          <Upload
            multiple
            accept=".csv"
            beforeUpload={() => false} // Prevent automatic upload
            onChange={({ fileList }) => handleFileChange(fileList)}
            showUploadList={true} // Hide default file list
          >
            <Button icon={<UploadOutlined />} disabled={loading}>
              Select Files
            </Button>
          </Upload>
        </div>
      </Modal>

      <style jsx>{`
        .spinner {
          border: 4px solid rgba(0, 0, 0, 0.1);
          border-left-color: #0070f3;
          border-radius: 50%;
          width: 20px;
          height: 20px;
          animation: spin 1s linear infinite;
          display: inline-block;
          margin-left: 10px;
        }
        @keyframes spin {
          0% {
            transform: rotate(0deg);
          }
          100% {
            transform: rotate(360deg);
          }
        }
      `}</style>
    </div>
  );
};

export default FileUpload;
