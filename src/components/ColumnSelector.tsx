import React, { useEffect, useState } from "react";
import {
  DndContext,
  closestCenter,
  KeyboardSensor,
  PointerSensor,
  useSensor,
  useSensors,
} from "@dnd-kit/core";
import {
  arrayMove,
  SortableContext,
  sortableKeyboardCoordinates,
  verticalListSortingStrategy,
} from "@dnd-kit/sortable";
import { SortableItem } from "./SortableItem";
import RightSidebarModal from "./RightSidebarModal";
import { COLUMN_MAPPING } from "./ColumnConst";

export type IColumnData = {
  id: string;
  title: string;
  checked: boolean;
  _id: string
};

interface ColumnSelectorProps {
  columns: IColumnData[];
  setIsSettingsModalVisible: (item: boolean) => void;
  handleUpdate: (item: IColumnData[]) => void;
  isSaving: boolean;
  isSettingsModalVisible: boolean;
}

const ColumnSelector: React.FC<ColumnSelectorProps> = ({
  columns,
  setIsSettingsModalVisible,
  isSettingsModalVisible,
  isSaving,
  handleUpdate,
}) => {
  const [items, setItems] = useState<IColumnData[]>(columns);
  const sensors = useSensors(
    useSensor(PointerSensor),
    useSensor(KeyboardSensor, {
      coordinateGetter: sortableKeyboardCoordinates,
    })
  );

  const handleDragEnd = (event: any) => {
    const { active, over } = event;
    if (!over) return;
    if (active.id !== over.id) {
      setItems((items) => {
        const oldIndex = items.findIndex((item) => item.id === active.id);
        const newIndex = items.findIndex((item) => item.id === over.id);
        return arrayMove(items, oldIndex, newIndex);
      });
    }
  };

  const handleCheckItem = (id: string, checked: boolean) => {
    const updatedItems = items.map((item) => {
      if (item.id === id) {
        return { ...item, checked };
      }
      return item;
    });
    setItems(updatedItems);
  };

  useEffect(() => {
    setItems(columns)
  }, [columns])

  return (
    <RightSidebarModal
      isVisible={isSettingsModalVisible}
      onClose={() => setIsSettingsModalVisible(false)}
    >
      <h2 className="my-2 text-center text-2xl font-bold leading-9 tracking-tight text-gray-900">
        Manage columns
      </h2>
      <DndContext
        sensors={sensors}
        collisionDetection={closestCenter}
        onDragEnd={handleDragEnd}
      >
        <SortableContext items={items} strategy={verticalListSortingStrategy}>
          <div className="p-4 max-w-md mx-auto space-y-2">
            {items.map((item) => (
              <SortableItem
                key={item._id}
                id={item.id}
                content={COLUMN_MAPPING[item.title as keyof typeof COLUMN_MAPPING]}
                checked={item.checked}
                handleCheckItem={handleCheckItem}
              />
            ))}
          </div>
        </SortableContext>
      </DndContext>
      <div className="w-full px-4 mb-4 flex justify-end">
        <button
          onClick={() => handleUpdate(items)}
          className="flex w-full max-w-[120px] justify-center items-center gap-1 rounded-md bg-indigo-600 px-3 py-1.5 text-sm font-semibold leading-6 text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
          disabled={isSaving}
        >
          {isSaving && (
            <i className="fa-solid fa-circle-notch animate-spin"></i>
          )}
          Save
        </button>
      </div>
    </RightSidebarModal>
  );
};

export default ColumnSelector;
