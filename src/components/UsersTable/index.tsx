import React, { useEffect, useMemo, useRef, useState } from "react";
import type { SyntheticListenerMap } from "@dnd-kit/core/dist/hooks/utilities";
import { useSortable } from "@dnd-kit/sortable";
import { CSS } from "@dnd-kit/utilities";
import { Avatar, Table } from "antd";
import type { TableColumnsType, TableProps } from "antd";
import UserEditButton from "../EditButton/UserEditButton";
import { INITIAL_PAGINATION } from "@/pages/leads";
import Loader from "../Loader";

type TableRowSelection<T> = TableProps<T>["rowSelection"];

interface RowContextProps {
  setActivatorNodeRef?: (element: HTMLElement | null) => void;
  listeners?: SyntheticListenerMap;
}

const RowContext = React.createContext<RowContextProps>({});

interface RowProps extends React.HTMLAttributes<HTMLTableRowElement> {
  "data-row-key": string;
}

const Row: React.FC<RowProps> = (props) => {
  const {
    attributes,
    listeners,
    setNodeRef,
    setActivatorNodeRef,
    transform,
    transition,
    isDragging,
  } = useSortable({ id: props["data-row-key"] });

  const style: React.CSSProperties = {
    ...props.style,
    transform: CSS.Translate.toString(transform),
    transition,
    ...(isDragging ? { position: "relative", zIndex: 9999 } : {}),
  };

  const contextValue = useMemo<RowContextProps>(
    () => ({ setActivatorNodeRef, listeners }),
    [setActivatorNodeRef, listeners]
  );

  return (
    <RowContext.Provider value={contextValue}>
      <tr {...props} ref={setNodeRef} style={style} {...attributes} />
    </RowContext.Provider>
  );
};

function UsersTable({ data, isLoading }: { data: User[]; isLoading: boolean }) {
  const [selectedRowKeys, setSelectedRowKeys] = useState<React.Key[]>([]);
  const elementRef = useRef<HTMLDivElement>(null);
  const [elementHeight, setElementHeight] = useState(0);
  const [sortedInfo, setSortedInfo] = useState<any>({});

  useEffect(() => {
    const updateHeight = () => {
      if (elementRef.current) {
        const height = elementRef.current.getBoundingClientRect().height;
        setElementHeight(height);
      }
    };

    const resizeObserver = new ResizeObserver(() => {
      updateHeight();
    });

    if (elementRef.current) {
      resizeObserver.observe(elementRef.current);
    }

    updateHeight();

    return () => {
      if (elementRef.current) {
        resizeObserver.unobserve(elementRef.current);
      }
    };
  }, []);

  useEffect(() => {}, [data]);

  const capitalizeFirstLetter = (str: string) => {
    return str.charAt(0).toUpperCase() + str.slice(1);
  };

  const columns: TableColumnsType<User> = [
    {
      title: "Avatar",
      dataIndex: "avatar",
      render: (_: any, record: User) => (
        <>
          <Avatar
            alt={record.name}
            src={record.avatar}
            size={"large"}
            className="text-lg font-semibold"
          >
            {!record.avatar && record.name?.charAt(0).toUpperCase()}
          </Avatar>
        </>
      ),
      ellipsis: true,
      width: 80,
    },
    {
      title: "Name",
      className: "long-column",
      dataIndex: "name",
      key: "name",
      sorter: (a, b) => {
        if (!a.name || !b.name) return 0;
        return a.name.localeCompare(b.name);
      },
      sortOrder: sortedInfo.columnKey === "name" && sortedInfo.order,
    },
    {
      title: "Email",
      className: "long-column",
      dataIndex: "email",
      key: "email",
      sorter: (a, b) => {
        if (!a.email || !b.email) return 0;
        return a.email.localeCompare(b.email);
      },
      sortOrder: sortedInfo.columnKey === "email" && sortedInfo.order,
    },
    {
      title: "Role",
      className: "long-column",
      dataIndex: "role",
      key: "role",
      sorter: (a, b) => {
        if (!a.role || !b.role) return 0;
        return a.role.localeCompare(b.role);
      },
      render: (text) => capitalizeFirstLetter(text.toLowerCase()),

      sortOrder: sortedInfo.columnKey === "role" && sortedInfo.order,
    },
    {
      title: "Action",
      dataIndex: "edit",
      width: 80,
      render: (_, record: User) => <UserEditButton user={record} />,
    },
  ];

  const onSelectChange = (newSelectedRowKeys: React.Key[]) => {
    console.log("selectedRowKeys changed: ", newSelectedRowKeys);
    setSelectedRowKeys(newSelectedRowKeys);
  };

  const handleTableChange = (pagination: any, filters: any, sorter: any) => {
    setSortedInfo(sorter);
  };

  const rowSelection: TableRowSelection<User> = {
    selectedRowKeys,
    onChange: onSelectChange,
  };

  return (
    <div ref={elementRef} className="flex-1 h-full overflow-hidden">
      {isLoading && <Loader />}
      <Table
        bordered
        rowKey="_id"
        components={{ body: { row: Row } }}
        rowSelection={rowSelection}
        columns={columns}
        pagination={false}
        dataSource={data}
        onChange={handleTableChange}
        scroll={{ y: elementHeight ? elementHeight - 130 : 0 }}
        className="shadow-md rounded-lg"
        locale={{ emptyText: isLoading ? " " : "No Data" }}
        sticky
      />
    </div>
  );
}

export default UsersTable;
