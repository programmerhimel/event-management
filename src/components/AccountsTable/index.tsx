import React, { useEffect, useMemo, useRef, useState } from "react";
import type { SyntheticListenerMap } from "@dnd-kit/core/dist/hooks/utilities";
import { useSortable } from "@dnd-kit/sortable";
import { CSS } from "@dnd-kit/utilities";
import { Table } from "antd";
import type { TableColumnsType, TableProps } from "antd";
import AccountEditButton from "../EditButton/AccountEditButton";
import Loader from "../Loader";

type TableRowSelection<T> = TableProps<T>["rowSelection"];

interface RowContextProps {
  setActivatorNodeRef?: (element: HTMLElement | null) => void;
  listeners?: SyntheticListenerMap;
}

const RowContext = React.createContext<RowContextProps>({});

interface RowProps extends React.HTMLAttributes<HTMLTableRowElement> {
  "data-row-key": string;
}

const Row: React.FC<RowProps> = (props) => {
  const {
    attributes,
    listeners,
    setNodeRef,
    setActivatorNodeRef,
    transform,
    transition,
    isDragging,
  } = useSortable({ id: props["data-row-key"] });

  const style: React.CSSProperties = {
    ...props.style,
    transform: CSS.Translate.toString(transform),
    transition,
    ...(isDragging ? { position: "relative", zIndex: 9999 } : {}),
  };

  const contextValue = useMemo<RowContextProps>(
    () => ({ setActivatorNodeRef, listeners }),
    [setActivatorNodeRef, listeners]
  );

  return (
    <RowContext.Provider value={contextValue}>
      <tr {...props} ref={setNodeRef} style={style} {...attributes} />
    </RowContext.Provider>
  );
};

function AccountsTable({
  data,
  userRole,
  isLoading
}: {
  data: Account[];
  userRole: "user" | "admin";
  isLoading: boolean
}) {
  const [selectedRowKeys, setSelectedRowKeys] = useState<React.Key[]>([]);
  const elementRef = useRef<HTMLDivElement>(null);
  const [elementHeight, setElementHeight] = useState(0);
  const actionColumn = {
    title: "Action",
    dataIndex: "edit",
    width: 80,
    render: (_: any, record: Account) => <AccountEditButton account={record} />,
  };
  const [sortedInfo, setSortedInfo] = useState<any>({});

  const capitalizeFirstLetter = (str: string) => {
    return str.charAt(0).toUpperCase() + str.slice(1);
  };

  const columns: TableColumnsType<Account> = [
    {
      className: 'long-column',
      title: "Name",
      dataIndex: "name",
      key: "name",
      sorter: (a, b) => {
        if (!a.name || !b.name) return 0;
        return a.name.localeCompare(b.name);
      },
      sortOrder: sortedInfo.columnKey === "name" && sortedInfo.order,
    },
    {
      className: 'long-column',
      title: "User",
      dataIndex: "userName",
    },
    {
      className: 'long-column',
      title: "Status",
      dataIndex: "status",
      key: "status",
      render: (text) => capitalizeFirstLetter(text.toLowerCase()),
      sorter: (a, b) => {
        if (!a.status || !b.status) return 0;
        return a.status.localeCompare(b.status);
      },
      sortOrder: sortedInfo.columnKey === "status" && sortedInfo.order,
    },
  ];

  useEffect(() => {
    const updateHeight = () => {
      if (elementRef.current) {
        const height = elementRef.current.getBoundingClientRect().height;
        setElementHeight(height);
      }
    };

    const resizeObserver = new ResizeObserver(() => {
      updateHeight();
    });

    if (elementRef.current) {
      resizeObserver.observe(elementRef.current);
    }

    updateHeight();

    return () => {
      if (elementRef.current) {
        resizeObserver.unobserve(elementRef.current);
      }
    };
  }, []);

  useEffect(() => {}, [data]);

  const onSelectChange = (newSelectedRowKeys: React.Key[]) => {
    setSelectedRowKeys(newSelectedRowKeys);
  };

  const handleTableChange = (pagination: any, filters: any, sorter: any) => {
    setSortedInfo(sorter);
  };

  const rowSelection: TableRowSelection<Account> = {
    selectedRowKeys,
    onChange: onSelectChange,
    // selections: [
    //   Table.SELECTION_ALL,
    //   Table.SELECTION_INVERT,
    //   Table.SELECTION_NONE,
    //   {
    //     key: "odd",
    //     text: "Select Odd Row",
    //     onSelect: (changeableRowKeys) => {
    //       let newSelectedRowKeys = [];
    //       newSelectedRowKeys = changeableRowKeys.filter((_, index) => {
    //         if (index % 2 !== 0) {
    //           return false;
    //         }
    //         return true;
    //       });
    //       setSelectedRowKeys(newSelectedRowKeys);
    //     },
    //   },
    //   {
    //     key: "even",
    //     text: "Select Even Row",
    //     onSelect: (changeableRowKeys) => {
    //       let newSelectedRowKeys = [];
    //       newSelectedRowKeys = changeableRowKeys.filter((_, index) => {
    //         if (index % 2 !== 0) {
    //           return true;
    //         }
    //         return false;
    //       });
    //       setSelectedRowKeys(newSelectedRowKeys);
    //     },
    //   },
    // ],
  };

  return (
    <div ref={elementRef} className="flex-1 h-full overflow-hidden">
      {isLoading && <Loader />}
      <Table
        bordered
        rowKey="_id"
        components={{ body: { row: Row } }}
        rowSelection={rowSelection}
        columns={[...columns, actionColumn]}
        dataSource={data}
        pagination={false}
        onChange={handleTableChange}
        scroll={{ y: elementHeight ? elementHeight - 130 : 0 }}
        className="shadow-md rounded-lg"
      />
    </div>
  );
}

export default AccountsTable;
