import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { TagsInput } from "react-tag-input-component";
import RightSidebarModal from "../RightSidebarModal";
import { Select } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { selectAllUsers } from "@/redux/selectors/usersSelectors";
import { Action, ThunkDispatch } from "@reduxjs/toolkit";
import { RootState } from "@/redux/reducers";
import { fetchUsers } from "@/redux/actions/usersActions";
import { fetchAccounts } from "@/redux/actions/accountsActions";

type LeadFormProps =
  | {
      isVisible: boolean;
      onClose: () => void;
      type: "add";
      lead?: undefined;
      user: { id: string; email: string; role: "user" | "admin"; name: string };
    }
  | {
      isVisible: boolean;
      onClose: () => void;
      type: "edit";
      lead: Lead | undefined | any;
      user: { id: string; email: string; role: "user" | "admin"; name: string };
    };

const actionOptions = ["open", "sent", "failed", "archived"];
const acceptOptions = [
  "open",
  "accepted",
  "active",
  "pending",
  "succeed",
  "failed",
  "archived",
];

const UnassignedOption = { name: "Unassigned", id: "" };

const LeadForm: React.FC<LeadFormProps> = ({
  isVisible,
  onClose,
  type,
  lead,
  user,
}) => {
  const [isAdding, setIsAdding] = useState(false);
  const [name, setName] = useState("");
  const [role, setRole] = useState("");
  const [url, setUrl] = useState("");
  const [country, setCountry] = useState("");
  const [avatar, setAvatar] = useState("");
  const [category, setCategory] = useState("");
  const [company, setCompany] = useState("");
  const [location, setLocation] = useState("");
  const [industry, setIndustry] = useState("");
  const [note, setNote] = useState("");
  const [tags, setTags] = useState<string[]>([]);
  const [resource, setResource] = useState("");
  const [actionStatus, setActionStatus] = useState("open");
  const [acceptStatus, setAcceptStatus] = useState("open");
  const allUsers = useSelector(selectAllUsers);
  const router = useRouter();
  const [userOptions, setUserOptions] = useState<
    { name: string; id: string }[]
  >([]);
  const [accountOptions, setAccountOptions] = useState<
    { name: string; id: string }[]
  >([]);
  const [assignedUser, setAssignedUser] = useState(lead?.assigned_user || "");
  const [assignedAccount, setAssignedAccount] = useState(
    lead?.assigned_account || ""
  );
  const dispatch = useDispatch<ThunkDispatch<RootState, any, Action>>();

  const handleSubmit = async (e: React.FormEvent) => {
    e.preventDefault();
    setIsAdding(true);

    const leadData = {
      name,
      role,
      url,
      country,
      avatar,
      category,
      company,
      location,
      industry,
      note,
      tags,
      resource,
      action_status: actionStatus,
      accept_status: acceptStatus,
      assigned_user: assignedUser === "" ? null : assignedUser,
      assigned_account: assignedUser === "" ? null : assignedAccount,
    };

    if (type === "add") {
      await addLead(leadData);
    } else if (type === "edit" && lead) {
      await editLead(lead._id, leadData);
    }
  };

  const addLead = async (data: Partial<Lead>) => {
    try {
      const res = await fetch("/api/leads", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      });

      if (res.ok) {
        setIsAdding(false);
        onClose();
      } else {
        console.error(await res.json());
      }
    } catch (error) {
      console.error(error);
    }
  };

  const editLead = async (id: string, data: Partial<Lead>) => {
    try {
      const res = await fetch(`/api/leads?id=${id}`, {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      });

      if (res.ok) {
        setIsAdding(false);
        onClose();
      } else {
        console.error(await res.json());
      }
    } catch (error) {
      console.error(error);
    }
  };

  const getUserOptions = () => {
    if (user?.role === "user") {
      setUserOptions([{ name: user?.name, id: user?.id }]);
    } else {
      const options: any = allUsers.map((usr) => ({
        name: usr.name,
        id: usr._id,
      }));
      options.push(UnassignedOption);
      setUserOptions(options);
    }
  };

  const handleAssignedUser = (value: any) => {
    if (!value) {
      setAssignedUser("");
    } else {
      setAssignedUser(value);
    }
    setAssignedAccount("");
  };

  const getAccountOptions = (user?: string) => {
    fetch(`/api/all-user-accounts?userId=${user || assignedUser}`)
      .then((res) => res.json())
      .then((accounts: Account[]) => {
        if (accounts?.length > 0) {
          const options = accounts.map((usr) => ({
            name: usr.name,
            id: usr._id,
          }));
          setAccountOptions(options);
        }
      });
  };

  useEffect(() => {
    if (assignedUser) {
      getAccountOptions();
    } else {
      setAccountOptions([]);
      setAssignedAccount("");
    }
  }, [assignedUser]);

  useEffect(() => {
    if (user?.role === "admin") {
      getUserOptions();
    }
    if (lead) {
      setName(lead.name);
      setRole(lead.role);
      setUrl(lead.url);
      setCountry(lead.country);
      setAvatar(lead.avatar);
      setCategory(lead.category);
      setCompany(lead.company);
      setLocation(lead.location);
      setIndustry(lead.industry);
      setNote(lead.note);
      setTags(lead.tags);
      setResource(lead.resource);
      setActionStatus(lead.action_status);
      setAcceptStatus(lead.accept_status);
      setAssignedUser(lead.assigned_user?._id);
      if (lead.assign_account) {
        setAssignedAccount(lead.assigned_account);
        getAccountOptions(lead.assigned_account);
      } else {
        setAssignedAccount("");
        setAccountOptions([]);
      }
    }
  }, [lead]);

  const allowEditing = user?.role === "admin" || type === "add";

  return (
    <RightSidebarModal isVisible={isVisible} onClose={onClose}>
      <form
        onSubmit={handleSubmit}
        className="w-full h-full flex flex-col justify-between items-center"
      >
        <div className="p-[30px] w-full overflow-y-auto">
          <h2 className="mb-10 text-center text-2xl font-bold leading-9 tracking-tight text-gray-900">
            {type === "add" ? "Add a" : "Edit"} lead
          </h2>
          <div className="space-y-6">
            <div>
              <label
                htmlFor="name"
                className="block text-sm font-medium leading-6 text-gray-900"
              >
                Name
              </label>
              <div className="mt-2">
                <input
                  id="name"
                  name="name"
                  type="text"
                  value={name}
                  onChange={(e) => setName(e.target.value)}
                  required
                  className="block w-full rounded-md border-0 py-1.5 px-2 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                  readOnly={!allowEditing}
                />
              </div>
            </div>
            {allowEditing ? (
              <>
                <div>
                  <label
                    htmlFor="role"
                    className="block text-sm font-medium leading-6 text-gray-900"
                  >
                    Role
                  </label>
                  <div className="mt-2">
                    <input
                      id="role"
                      name="role"
                      type="text"
                      value={role}
                      onChange={(e) => setRole(e.target.value)}
                      className="block w-full rounded-md border-0 py-1.5 px-2 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                      readOnly={!allowEditing}
                    />
                  </div>
                </div>

                <div>
                  <label
                    htmlFor="url"
                    className="block text-sm font-medium leading-6 text-gray-900"
                  >
                    URL
                  </label>
                  <div className="mt-2">
                    <input
                      id="url"
                      name="url"
                      type="url"
                      value={url}
                      onChange={(e) => setUrl(e.target.value)}
                      required
                      className="block w-full rounded-md border-0 py-1.5 px-2 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                      readOnly={!allowEditing}
                    />
                  </div>
                </div>

                <div>
                  <label
                    htmlFor="country"
                    className="block text-sm font-medium leading-6 text-gray-900"
                  >
                    Country
                  </label>
                  <div className="mt-2">
                    <input
                      id="country"
                      name="country"
                      type="text"
                      value={country}
                      onChange={(e) => setCountry(e.target.value)}
                      className="block w-full rounded-md border-0 py-1.5 px-2 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                      readOnly={!allowEditing}
                    />
                  </div>
                </div>

                <div>
                  <label
                    htmlFor="avatar"
                    className="block text-sm font-medium leading-6 text-gray-900"
                  >
                    Avatar
                  </label>
                  <div className="mt-2">
                    <input
                      id="avatar"
                      name="avatar"
                      type="text"
                      value={avatar}
                      onChange={(e) => setAvatar(e.target.value)}
                      className="block w-full rounded-md border-0 py-1.5 px-2 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                      readOnly={!allowEditing}
                    />
                  </div>
                </div>

                <div>
                  <label
                    htmlFor="category"
                    className="block text-sm font-medium leading-6 text-gray-900"
                  >
                    Category
                  </label>
                  <div className="mt-2">
                    <input
                      id="category"
                      name="category"
                      type="text"
                      value={category}
                      onChange={(e) => setCategory(e.target.value)}
                      className="block w-full rounded-md border-0 py-1.5 px-2 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                      readOnly={!allowEditing}
                    />
                  </div>
                </div>

                <div>
                  <label
                    htmlFor="company"
                    className="block text-sm font-medium leading-6 text-gray-900"
                  >
                    Company
                  </label>
                  <div className="mt-2">
                    <input
                      id="company"
                      name="company"
                      type="text"
                      value={company}
                      onChange={(e) => setCompany(e.target.value)}
                      className="block w-full rounded-md border-0 py-1.5 px-2 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                      readOnly={!allowEditing}
                    />
                  </div>
                </div>

                <div>
                  <label
                    htmlFor="location"
                    className="block text-sm font-medium leading-6 text-gray-900"
                  >
                    Location
                  </label>
                  <div className="mt-2">
                    <input
                      id="location"
                      name="location"
                      type="text"
                      value={location}
                      onChange={(e) => setLocation(e.target.value)}
                      className="block w-full rounded-md border-0 py-1.5 px-2 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                      readOnly={!allowEditing}
                    />
                  </div>
                </div>

                <div>
                  <label
                    htmlFor="industry"
                    className="block text-sm font-medium leading-6 text-gray-900"
                  >
                    Industry
                  </label>
                  <div className="mt-2">
                    <input
                      id="industry"
                      name="industry"
                      type="text"
                      value={industry}
                      onChange={(e) => setIndustry(e.target.value)}
                      className="block w-full rounded-md border-0 py-1.5 px-2 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                      readOnly={!allowEditing}
                    />
                  </div>
                </div>
              </>
            ) : null}
            <div>
              <label
                htmlFor="note"
                className="block text-sm font-medium leading-6 text-gray-900"
              >
                Note
              </label>
              <div className="mt-2">
                <textarea
                  id="note"
                  name="note"
                  value={note}
                  onChange={(e) => setNote(e.target.value)}
                  className="block w-full min-h-40 max-h-52 rounded-md border-0 py-1.5 px-2 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                />
              </div>
            </div>

            <div>
              <label
                htmlFor="tags"
                className="block text-sm font-medium leading-6 text-gray-900"
              >
                Tags
              </label>
              <div className="mt-2">
                <TagsInput
                  value={tags}
                  onChange={(tags: string[]) => setTags(tags)}
                />
              </div>
            </div>
            {allowEditing ? (
              <div>
                <label
                  htmlFor="resource"
                  className="block text-sm font-medium leading-6 text-gray-900"
                >
                  Resource
                </label>
                <div className="mt-2">
                  <input
                    id="resource"
                    name="resource"
                    type="text"
                    value={resource}
                    onChange={(e) => setResource(e.target.value)}
                    className="block w-full rounded-md border-0 py-1.5 px-2 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                    readOnly={!allowEditing}
                  />
                </div>
              </div>
            ) : null}

            <div>
              <label
                htmlFor="actionStatus"
                className="block text-sm font-medium leading-6 text-gray-900"
              >
                Action Status
              </label>
              <div className="mt-2">
                <Select
                  id="actionStatus"
                  value={actionStatus}
                  className="w-full"
                  onChange={(value) => setActionStatus(value)}
                >
                  {actionOptions.map((item, index) => (
                    <Select.Option
                      key={index}
                      value={item}
                      className="capitalize"
                    >
                      {item}
                    </Select.Option>
                  ))}
                </Select>
              </div>
            </div>

            <div>
              <label
                htmlFor="acceptStatus"
                className="block text-sm font-medium leading-6 text-gray-900"
              >
                Accept Status
              </label>
              <div className="mt-2">
                <Select
                  id="acceptStatus"
                  value={acceptStatus}
                  className="w-full"
                  onChange={(value) => setAcceptStatus(value)}
                >
                  {acceptOptions.map((item, index) => (
                    <Select.Option
                      key={index}
                      value={item}
                      className="capitalize"
                    >
                      {item}
                    </Select.Option>
                  ))}
                </Select>
              </div>
            </div>
            {allowEditing ? (
              <>
                <div>
                  <label
                    htmlFor="assignedUser"
                    className="block text-sm font-medium leading-6 text-gray-900"
                  >
                    Assigned User
                  </label>
                  <div className="mt-2">
                    <Select
                      id="assignedUser"
                      value={assignedUser}
                      className="w-full"
                      onChange={(value) => setAssignedUser(value)}
                      options={userOptions.map((user) => ({
                        label: user?.name,
                        value: user?.id,
                      }))}
                      disabled={!allowEditing}
                    />
                  </div>
                </div>

                <div>
                  <label
                    htmlFor="assignedAccount"
                    className="block text-sm font-medium leading-6 text-gray-900"
                  >
                    Assigned Account
                  </label>
                  <div className="mt-2">
                    <Select
                      id="assignedAccount"
                      value={assignedAccount}
                      className="w-full"
                      onChange={(value) => setAssignedAccount(value)}
                      options={accountOptions
                        .map((account) => ({
                          label: account.name,
                          value: account.id,
                        }))
                        .concat([{ label: "Unassigned", value: "" }])}
                      disabled={!allowEditing}
                    />
                  </div>
                </div>
              </>
            ) : null}
            <div className="flex justify-end items-center mt-6">
              <button
                type="submit"
                className="w-full max-w-[150px] py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                disabled={isAdding}
              >
                {isAdding ? "Submitting..." : "Submit"}
              </button>
            </div>
          </div>
        </div>
      </form>
    </RightSidebarModal>
  );
};

export default LeadForm;
