import { INITIAL_PAGINATION } from '@/pages/leads';
import { Card, Pagination, Select, Space } from 'antd';

const StickyPagination = ({
  pagination,
  setPagination
}: {
  pagination: typeof INITIAL_PAGINATION;
  setPagination: React.Dispatch<
    React.SetStateAction<typeof INITIAL_PAGINATION>
  >;
}) => {
  const generatePageSizeOptions = (sizes: string[]) => {
    return sizes.map((size) => ({
      value: Number(size),
      label: `${size} / page`
    }));
  };

  return (
    <Card
      classNames={{
        body: '!py-[12px] !px-[16px]'
      }}
    >
      <Space className="w-full justify-between">
        <div className="flex items-center gap-3">
          <div className="text-base">Rows per page:</div>
          <Select
            showSearch
            placeholder="Select page size"
            optionFilterProp="label"
            options={generatePageSizeOptions(pagination.pageSizeOptions)}
            defaultValue={pagination.pageSize}
            onChange={(size) => {
              setPagination({ ...pagination, current: 1, pageSize: size });
            }}
            size="large"
          />
        </div>

        <Pagination
          {...pagination}
          showSizeChanger={false}
          onChange={(page, pageSize) => {
            let current = page;
            if (pageSize !== pagination.pageSize) current = 1;
            setPagination({ ...pagination, current, pageSize });
          }}
        />
      </Space>
    </Card>
  );
};

export default StickyPagination;
