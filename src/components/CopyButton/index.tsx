import { Tooltip } from "antd";
import React, { useState } from "react";

function Copybutton({ url }: { url: string }) {
  const [isCopied, setIsCopied] = useState(false);

  const handleCopy = () => {
    navigator.clipboard.writeText(url).then(
      () => {
        setIsCopied(true);
        setTimeout(() => {
          setIsCopied(false);
        }, 3000);
      },
      (err) => {
        console.error("Failed to copy: ", err);
      }
    );
  };
  return (
    <div className="flex gap-2 w-full justify-between items-center">
      <p>{url}</p>
      <Tooltip title={isCopied ? "Copied" : "Copy"}>
        <button onClick={handleCopy}>
          <i className="fa-solid fa-clipboard text-base text-gray-600 hover:text-gray-700 hover:scale-110"></i>
        </button>
      </Tooltip>
    </div>
  );
}

export default Copybutton;
