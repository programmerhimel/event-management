import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCircleNotch } from '@fortawesome/free-solid-svg-icons';

const Loader = () => {
  return (
    <div className="absolute inset-0 flex items-center justify-center bg-transparent bg-opacity-100  z-[10000]">
      <FontAwesomeIcon icon={faCircleNotch} spin size="3x" className="text-slate-600" />
    </div>
  );
};

export default Loader;
