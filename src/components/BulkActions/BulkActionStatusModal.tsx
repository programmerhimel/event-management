import { Button, Modal, Select } from "antd";
import React, { useState } from "react";

type ModalProps = {
  bulkModalOpen: boolean;
  handleBulkUpdate: (payload: any) => Promise<void>;
  handleCancel: () => void;
};

export const BulkActionStatusModal = ({
  bulkModalOpen,
  handleBulkUpdate,
  handleCancel,
}: ModalProps) => {
  const [actionStatus, setActionStatus] = useState("");
  const actionOptions = ["open", "sent", "failed", "archived"];

  const handleSubmit = () => {
    handleBulkUpdate({ actionStatus });
  };

  return (
    <Modal
      title="Update action status"
      open={bulkModalOpen}
      onOk={handleSubmit}
      onCancel={handleCancel}
      footer={[
        <Button key="back" onClick={handleCancel}>
          cancel
        </Button>,
        <Button
          key="submit"
          disabled={!actionStatus}
          type="primary"
          onClick={handleSubmit}
        >
          Submit
        </Button>,
      ]}
    >
      <div>
        <label
          htmlFor="actionStatus"
          className="block text-sm font-medium leading-6 text-gray-900"
        >
          Action Status
        </label>
        <div className="mt-2">
          <Select
            id="actionStatus"
            value={actionStatus}
            className="w-full"
            onChange={(value) => setActionStatus(value)}
          >
            {actionOptions.map((item, index) => (
              <Select.Option key={index} value={item} className="capitalize">
                {item}
              </Select.Option>
            ))}
          </Select>
        </div>
      </div>
    </Modal>
  );
};
