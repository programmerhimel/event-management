import { Button, Modal, Select } from "antd";
import React, { useEffect, useState } from "react";

type ModalProps = {
  bulkModalOpen: boolean,
  handleBulkUpdate: (payload: any) => Promise<void>,
  handleCancel: () => void
}

export const BulkAssignedModal = ({bulkModalOpen, handleBulkUpdate, handleCancel}: ModalProps) => {
  const [assignedUser, setAssignedUser] = useState("");
  const [assignedAccount, setAssignedAccount] = useState("");
  const [userOptions, setUserOptions] = useState<{name: string, id: string}[]>([])
  const [accountOptions, setAccountOptions] = useState<{name: string, id: string}[]>([])

  const getUserOptions = () => {
    fetch("/api/get-options?module=user")
    .then((res) => res.json())
    .then((data: User[]) => {
      const options = data.map((usr) => ({name: usr.name, id: usr._id}))
      setUserOptions(options)
    });
  }

  const getAccountOptions = (user?: string) => {
    fetch(`/api/accounts?userId=${user || assignedUser}&page=1&top=50`)
    .then((res) => res.json())
    .then((data: {accounts: User[]}) => {
      if (data.accounts.length) {
        const options = data?.accounts.map((usr) => ({name: usr.name, id: usr._id}))
        setAccountOptions(options)
      }
    });
  }

  useEffect(() => {
    getUserOptions()
  }, [])

  const handleSubmit = () => {
    handleBulkUpdate({assignedUser, assignedAccount})
  }

  useEffect(() => {
    if (assignedUser) getAccountOptions()
  }, [assignedUser])

  return (
    <Modal
      title="Bulk Assign User and Account"
      open={bulkModalOpen}
      onOk={handleSubmit}
      onCancel={handleCancel}
      centered
      footer={[
        <Button key="back" onClick={handleCancel}>
          Cancel
        </Button>,
        <Button
          key="submit"
          disabled={!assignedAccount}
          type="primary"
          onClick={handleSubmit}
        >
          Submit
        </Button>
      ]}
    >
      <div>
        <label
          htmlFor="user"
          className="block text-sm font-medium leading-6 text-gray-900 mt-3"
        >
          Assigned User
        </label>
        <Select
          className="mb-3"
          style={{ width: '100%' }}
          onChange={(value: string) => {
            if (value !== assignedUser) {
              setAssignedUser(value);
              setAssignedAccount("");
            }
          }}
          value={assignedUser}
          options={[
            {
              label: <span>User</span>,
              title: "manager",
              options: userOptions.map((item) => ({
                label: item.name,
                value: item.id
              }))
            }
          ]}
        />
      </div>
      <div>
        <label
          htmlFor="user"
          className="block text-sm font-medium leading-6 text-gray-900"
        >
          Assigned Account
        </label>
        <Select
          style={{ width: "100%" }}
          onChange={(value: string) => {
            setAssignedAccount(value);
          }}
          value={assignedAccount}
          disabled={!assignedUser}
          options={[
            {
              label: <span>Account</span>,
              title: "manager",
              options: accountOptions.map((item) => ({
                label: item.name,
                value: item.id,
              })),
            },
          ]}
        />
      </div>
    </Modal>
  );
};
