// components/RightSidebarModal.tsx
import React from "react";

type RightSidebarModalProps = {
  isVisible: boolean;
  onClose: () => void;
  children: React.ReactNode;
};

const RightSidebarModal: React.FC<RightSidebarModalProps> = ({
  isVisible,
  onClose,
  children,
}) => {
  return (
    <div
      className={`modal-overlay right-0 ${isVisible ? "visible" : ""}`}
      onClick={onClose}
    >
      <div
        className={`modal-content ${isVisible ? "visible" : ""}`}
        onClick={(e) => e.stopPropagation()}
      >
        <button className={`close-button right-5`} onClick={onClose}>
          ×
        </button>
        {children}
      </div>
    </div>
  );
};

export default RightSidebarModal;
