import React from "react";
import { useSortable } from "@dnd-kit/sortable";
import { CSS } from "@dnd-kit/utilities";
import { Checkbox } from "antd";

type SortableItemProps = {
  id: string;
  content: string;
  checked: boolean;
  handleCheckItem: (id: string, checked: boolean) => void;
};

export const SortableItem: React.FC<SortableItemProps> = ({
  id,
  content,
  checked,
  handleCheckItem,
}) => {
  const { attributes, listeners, setNodeRef, transform, transition } =
    useSortable({ id });

  const style = {
    transform: CSS.Transform.toString(transform),
    transition,
    padding: "2px",
    margin: "0px",
    backgroundColor: "#ffffff",
    borderRadius: "4px",
  };
  const onPointerDown = (event: any) => {
    if ((event.target as HTMLInputElement).type === 'checkbox') return
    listeners?.onPointerDown(event)
  }
  return (
    <div ref={setNodeRef} style={style} {...attributes} {...listeners} onPointerDown={onPointerDown}>
      <Checkbox
        className="bg-white hover:bg-gray-50 px-3 py-2 w-full rounded"
        checked={checked}
        onChange={(e: any) => {
          handleCheckItem(id, !checked);
        }}
      >
        {content}
      </Checkbox>
    </div>
  );
};
