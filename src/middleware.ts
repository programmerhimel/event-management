import { withAuth } from "next-auth/middleware";
import type { NextRequest } from "next/server";

export default withAuth({
  callbacks: {
    authorized: async ({ req, token }) => {
      const pathname = req.nextUrl.pathname;

      if (token) return true;
      if (pathname === "/api/registerUser" || pathname === "/swagger/docs") return true;

      return false;
    },
  },
  pages: {
    signIn: "/signin",
    // error: "/error",
  },
});
