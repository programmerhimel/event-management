import { createContext } from "react";

export const ClientContext = createContext(undefined as any);
