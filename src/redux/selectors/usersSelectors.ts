import { RootState } from '../reducers';

export const selectAllUsers = (state: RootState) => state.users.data;
export const selectUsersStatus = (state: RootState) => state.users.status;
export const selectUsersError = (state: RootState) => state.users.error;
