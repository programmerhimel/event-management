import { RootState } from '../reducers';

export const selectAllAccounts = (state: RootState) => state.accounts.data;
export const selectAccountsStatus = (state: RootState) => state.accounts.status;
export const selectAccountsError = (state: RootState) => state.accounts.error;
