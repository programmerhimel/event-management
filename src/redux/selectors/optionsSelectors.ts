import { RootState } from '../reducers';

export const selectUserOptions = (state: RootState) =>
  state.options.userOptions;
export const selectCountryOptions = (state: RootState) =>
  state.options.countryOptions;
export const selectCategoryOptions = (state: RootState) =>
  state.options.categoryOptions;
