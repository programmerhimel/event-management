import { RootState } from '../reducers';

export const selectAllLeads = (state: RootState) => state.leads.data;
export const selectLeadsStatus = (state: RootState) => state.leads.status;
export const selectLeadsError = (state: RootState) => state.leads.error;
export const selectColumnInfo = (state: RootState) => state.leads.columnInfo;