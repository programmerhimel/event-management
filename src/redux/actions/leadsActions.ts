import { createAsyncThunk } from '@reduxjs/toolkit';
import axios from 'axios';

export const fetchLeads = createAsyncThunk<Lead[]>('leads/fetchLeads', async () => {
  try {
    const response = await axios.get('/api/leads');
    return response.data;
  } catch (error) {
    throw error;
  }
});

export const fetchColumnInfo = createAsyncThunk<any[]>('leads/lead-column-info', async () => {
  try {
    const response = await axios.get('/api/lead-column-info');
    return response.data;
  } catch (error) {
    throw error;
  }
});
