import { createAsyncThunk } from '@reduxjs/toolkit';
import axios from 'axios';

export const fetchUserOptions = createAsyncThunk<User[]>(
  'options/fetchUserOptions',
  async () => {
    try {
      const response = await axios.get('/api/get-options?module=user');
      console.log('res =>', response);
      return response.data;
    } catch (error) {
      throw error;
    }
  }
);

export const fetchCountryOptions = createAsyncThunk<any[]>(
  'options/fetchCountryOptions',
  async () => {
    try {
      const response = await axios.get('/api/get-options?module=country');
      return response.data;
    } catch (error) {
      throw error;
    }
  }
);

export const fetchCategoryOptions = createAsyncThunk<any[]>(
  'options/fetchCategoryOptions',
  async () => {
    try {
      const response = await axios.get('/api/get-options?module=category');
      return response.data;
    } catch (error) {
      throw error;
    }
  }
);

export const fetchAllOptions = createAsyncThunk<
  { countries: any[], categories: any[], users: User[] }
>('options/fetchAllOptions', async () => {
  try {
    const response = await axios.get('/api/get-options?module=all');
    return response.data;
  } catch (error) {
    throw error;
  }
});