import { createAsyncThunk } from '@reduxjs/toolkit';
import axios from 'axios';
import { Account } from '../../../models/Account';


export const fetchAccounts = createAsyncThunk<Account[]>('accounts/fetchAccounts', async () => {
  try {
    const response = await axios.get('/api/all-user-accounts');
    return response.data;
  } catch (error) {
    throw error;
  }
});

export const updateAccount = createAsyncThunk<Account, Partial<Account>>(
  'accounts/updateAccount',
  async (updatedAccount: Partial<Account>) => {
    try {
      const response = await axios.put(`/api/accounts/${updatedAccount._id}`, updatedAccount);
      return response.data;
    } catch (error) {
      throw error;
    }
  }
);

export const createAccount = createAsyncThunk<Account, Partial<Account>>(
  'accounts/createAccount',
  async (newAccount: Partial<Account>) => {
    try {
      const response = await axios.post('/api/accounts', newAccount);
      return response.data;
    } catch (error) {
      throw error;
    }
  }
);

