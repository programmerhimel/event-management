import { createAsyncThunk } from '@reduxjs/toolkit';
import axios from 'axios';
import { User } from '../../../models/User';

export const fetchUsers = createAsyncThunk<User[]>('users/fetchUsers', async () => {
    try{
      const response = await axios.get('/api/all-users');
      return response.data;
    } catch (error) {
      throw error;
    }
});

export const updateUser = createAsyncThunk<User, { id: string, user: Partial<User> }>(
  'users/updateUser',
  async ({ id, user }) => {
    const response = await axios.put(`/api/users/${id}`, user);
    return response.data;
  }
);

export const createUser = createAsyncThunk<User, Partial<User>>(
  'users/createUser',
  async (user) => {
    const response = await axios.post('/api/users', user);
    return response.data;
  }
);