import { combineReducers } from '@reduxjs/toolkit';
import leadsReducer from './leadsReducer';
import usersReducer from './usersReducer';
import accountsReducer from './accountsReducer';
import optionsReducer from './optionsReducer';

const rootReducer = combineReducers({
  leads: leadsReducer,
  users: usersReducer,
  accounts: accountsReducer,
  options: optionsReducer,
});

export type RootState = ReturnType<typeof rootReducer>;
export default rootReducer;
