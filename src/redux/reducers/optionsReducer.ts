import { createSlice } from '@reduxjs/toolkit';
import {
  fetchUserOptions,
  fetchCategoryOptions,
  fetchCountryOptions,
  fetchAllOptions
} from '../actions/optionsActions';

interface OptionsState {
  countryOptions: any[];
  userOptions: User[];
  categoryOptions: any[];

  userOptionsStatus: 'idle' | 'loading' | 'succeeded' | 'failed';
  countryOptionsStatus: 'idle' | 'loading' | 'succeeded' | 'failed';
  categoryOptionsStatus: 'idle' | 'loading' | 'succeeded' | 'failed';

  countryOptionsError: string | null;
  userOptionsError: string | null;
  categoryOptionsError: string | null;

  allOptionsStatus: 'idle' | 'loading' | 'succeeded' | 'failed';
  allOptionsError: string | null;
}

const initialState: OptionsState = {
  countryOptions: [],
  userOptions: [],
  categoryOptions: [],

  userOptionsStatus: 'idle',
  countryOptionsStatus: 'idle',
  categoryOptionsStatus: 'idle',

  countryOptionsError: null,
  userOptionsError: null,
  categoryOptionsError: null,

  allOptionsStatus: 'idle',
  allOptionsError: null
};

const optionsSlice = createSlice({
  name: 'options',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(fetchUserOptions.pending, (state) => {
        state.userOptionsStatus = 'loading';
      })
      .addCase(fetchUserOptions.fulfilled, (state, action) => {
        state.userOptionsStatus = 'succeeded';
        state.userOptions = action.payload;
      })
      .addCase(fetchUserOptions.rejected, (state, action) => {
        state.userOptionsStatus = 'failed';
        state.userOptionsError = action.error.message ?? 'Unknown error';
      })
      .addCase(fetchCountryOptions.pending, (state) => {
        state.countryOptionsStatus = 'loading';
      })
      .addCase(fetchCountryOptions.fulfilled, (state, action) => {
        state.countryOptionsStatus = 'succeeded';
        state.countryOptions = action.payload;
      })
      .addCase(fetchCountryOptions.rejected, (state, action) => {
        state.countryOptionsStatus = 'failed';
        state.countryOptionsError = action.error.message ?? 'Unknown error';
      })
      .addCase(fetchCategoryOptions.pending, (state) => {
        state.categoryOptionsStatus = 'loading';
      })
      .addCase(fetchCategoryOptions.fulfilled, (state, action) => {
        state.categoryOptionsStatus = 'succeeded';
        state.categoryOptions = action.payload;
      })
      .addCase(fetchCategoryOptions.rejected, (state, action) => {
        state.categoryOptionsStatus = 'failed';
        state.categoryOptionsError = action.error.message ?? 'Unknown error';
      })
      .addCase(fetchAllOptions.pending, (state) => {
        state.allOptionsStatus = 'loading';
      })
      .addCase(fetchAllOptions.fulfilled, (state, action) => {
        state.allOptionsStatus = 'succeeded';
        state.countryOptions = action.payload.countries;
        state.categoryOptions = action.payload.categories;
        state.userOptions = action.payload.users;
      })
      .addCase(fetchAllOptions.rejected, (state, action) => {
        state.allOptionsStatus = 'failed';
        state.allOptionsError = action.error.message ?? 'Unknown error';
      });
  }
});

export default optionsSlice.reducer;
