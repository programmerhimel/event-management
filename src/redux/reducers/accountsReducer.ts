import { createSlice } from '@reduxjs/toolkit';
import { fetchAccounts, updateAccount, createAccount } from '../actions/accountsActions';
import { Account } from '../../../models/Account';

interface AccountsState {
  data: Account[];
  status: 'idle' | 'loading' | 'succeeded' | 'failed';
  error: string | null;
}

const initialState: AccountsState = {
  data: [],
  status: 'idle',
  error: null,
};

const accountsSlice = createSlice({
  name: 'accounts',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(fetchAccounts.pending, (state) => {
        state.status = 'loading';
      })
      .addCase(fetchAccounts.fulfilled, (state, action) => {
        state.status = 'succeeded';
        // @ts-ignore
        state.data = action.payload;
      })
      .addCase(fetchAccounts.rejected, (state, action) => {
        state.status = 'failed';
        state.error = action.error.message ?? 'Unknown error';
      })
      .addCase(updateAccount.pending, (state) => {
        state.status = 'loading';
      })
      .addCase(updateAccount.fulfilled, (state, action) => {
        state.status = 'succeeded';
        // @ts-ignore
        state.data = state.data.map((account) =>
          account._id === action.payload._id ? action.payload : account
        );
      })
      .addCase(updateAccount.rejected, (state, action) => {
        state.status = 'failed';
        state.error = action.error.message ?? 'Unknown error';
      })
      .addCase(createAccount.pending, (state) => {
        state.status = 'loading';
      })
      .addCase(createAccount.fulfilled, (state, action) => {
        state.status = 'succeeded';
        // @ts-ignore
        state.data.push(action.payload);
      })
      .addCase(createAccount.rejected, (state, action) => {
        state.status = 'failed';
        state.error = action.error.message ?? 'Unknown error';
      });
  },
});

export default accountsSlice.reducer;
