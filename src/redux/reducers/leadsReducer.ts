import { createSlice } from '@reduxjs/toolkit';
import { fetchLeads, fetchColumnInfo } from '../actions/leadsActions';
import { Lead } from '../../../models/Lead';

interface LeadsState {
  data: Lead[];
  status: 'idle' | 'loading' | 'succeeded' | 'failed';
  columnInfo: any[];
  error: string | null;
}

const initialState: LeadsState = {
  data: [],
  status: 'idle',
  columnInfo: [],
  error: null,
};

const leadsSlice = createSlice({
  name: 'leads',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(fetchLeads.pending, (state) => {
        state.status = 'loading';
      })
      .addCase(fetchLeads.fulfilled, (state, action) => {
        state.status = 'succeeded';
        // @ts-ignore
        state.data = action.payload;
      })
      .addCase(fetchLeads.rejected, (state, action) => {
        state.status = 'failed';
        // @ts-ignore
        state.error = action.error.message;
      })
      .addCase(fetchColumnInfo.pending, (state) => {
        state.status = 'loading';
      })
      .addCase(fetchColumnInfo.fulfilled, (state, action) => {
        state.status = 'succeeded';
        // @ts-ignore
        state.data = action.payload;
      })
      .addCase(fetchColumnInfo.rejected, (state, action) => {
        state.status = 'failed';
        // @ts-ignore
        state.error = action.error.message;
      });
  },
});

export default leadsSlice.reducer;
