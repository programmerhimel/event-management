"use client";

import { GetStaticProps, InferGetStaticPropsType } from "next";
import { createSwaggerSpec } from "next-swagger-doc";
import dynamic from "next/dynamic";
import "swagger-ui-react/swagger-ui.css";
import {
  AcceptStatusEnum,
  AccountStatusEnum,
  ActionStatusEnum,
} from "../../../models/enumModels";

const SwaggerUI = dynamic<{
  spec: any;
}>(
  // @ts-ignore
  () => import("swagger-ui-react"),
  { ssr: false }
);

function ApiDoc({ spec }: InferGetStaticPropsType<typeof getStaticProps>) {
  console.log(spec);
  return <SwaggerUI spec={spec} />;
}

export const getStaticProps: GetStaticProps = async () => {
  try {
    const spec: Record<string, any> = createSwaggerSpec({
      definition: {
        openapi: "3.0.0",
        info: {
          title: "aviaai Swagger API Example",
          version: "1.0",
        },
        paths: {
          "/api/accounts": {
            get: {
              summary: "Gets a list of accounts",
              tags: ["Accounts"],
              responses: {
                "200": {
                  description: "The list of accounts",
                  content: {
                    "application/json": {
                      schema: {
                        type: "array",
                        items: {
                          $ref: "#/components/schemas/Account",
                        },
                      },
                    },
                  },
                },
                "500": {
                  description: "Error fetching accounts",
                },
              },
            },
            post: {
              summary: "Creates a new account",
              tags: ["Accounts"],
              requestBody: {
                required: true,
                content: {
                  "application/json": {
                    schema: {
                      $ref: "#/components/schemas/Account",
                    },
                  },
                },
              },
              responses: {
                "201": {
                  description: "The created account",
                  content: {
                    "application/json": {
                      schema: {
                        $ref: "#/components/schemas/Account",
                      },
                    },
                  },
                },
                "500": {
                  description: "Error creating account",
                },
              },
            },
          },
          "/api/accounts/{id}": {
            put: {
              summary: "Updates an existing account",
              tags: ["Accounts"],
              parameters: [
                {
                  in: "path",
                  name: "id",
                  schema: {
                    type: "string",
                  },
                  required: true,
                  description: "The account id",
                },
              ],
              requestBody: {
                required: true,
                content: {
                  "application/json": {
                    schema: {
                      $ref: "#/components/schemas/Account",
                    },
                  },
                },
              },
              responses: {
                "200": {
                  description: "The updated account",
                  content: {
                    "application/json": {
                      schema: {
                        $ref: "#/components/schemas/Account",
                      },
                    },
                  },
                },
                "404": {
                  description: "Account not found",
                },
                "500": {
                  description: "Error updating account",
                },
              },
            },
            delete: {
              summary: "Deletes an existing account",
              tags: ["Accounts"],
              parameters: [
                {
                  in: "path",
                  name: "id",
                  schema: {
                    type: "string",
                  },
                  required: true,
                  description: "The account id",
                },
              ],
              responses: {
                "200": {
                  description: "Account deleted successfully",
                },
                "404": {
                  description: "Account not found",
                },
                "500": {
                  description: "Error deleting account",
                },
              },
            },
          },
          "/api/leads": {
            get: {
              summary: "Gets a list of leads",
              tags: ["Leads"],
              responses: {
                "200": {
                  description: "The list of leads",
                  content: {
                    "application/json": {
                      schema: {
                        type: "array",
                        items: {
                          $ref: "#/components/schemas/Lead",
                        },
                      },
                    },
                  },
                },
                "500": {
                  description: "Error fetching leads",
                },
              },
            },
            post: {
              summary: "Creates a new lead",
              tags: ["Leads"],
              requestBody: {
                required: true,
                content: {
                  "application/json": {
                    schema: {
                      $ref: "#/components/schemas/Lead",
                    },
                  },
                },
              },
              responses: {
                "201": {
                  description: "The created lead",
                  content: {
                    "application/json": {
                      schema: {
                        $ref: "#/components/schemas/Lead",
                      },
                    },
                  },
                },
                "500": {
                  description: "Error creating lead",
                },
              },
            },
          },
          "/api/leads/{id}": {
            put: {
              summary: "Updates an existing lead",
              tags: ["Leads"],
              parameters: [
                {
                  in: "path",
                  name: "id",
                  schema: {
                    type: "string",
                  },
                  required: true,
                  description: "The lead id",
                },
              ],
              requestBody: {
                required: true,
                content: {
                  "application/json": {
                    schema: {
                      $ref: "#/components/schemas/Lead",
                    },
                  },
                },
              },
              responses: {
                "200": {
                  description: "The updated lead",
                  content: {
                    "application/json": {
                      schema: {
                        $ref: "#/components/schemas/Lead",
                      },
                    },
                  },
                },
                "404": {
                  description: "Lead not found",
                },
                "500": {
                  description: "Error updating lead",
                },
              },
            },
            delete: {
              summary: "Deletes an existing lead",
              tags: ["Leads"],
              parameters: [
                {
                  in: "path",
                  name: "id",
                  schema: {
                    type: "string",
                  },
                  required: true,
                  description: "The lead id",
                },
              ],
              responses: {
                "200": {
                  description: "Lead deleted successfully",
                },
                "404": {
                  description: "Lead not found",
                },
                "500": {
                  description: "Error deleting lead",
                },
              },
            },
          },
          "/api/users": {
            get: {
              summary: "Gets a list of users",
              tags: ["Users"],
              responses: {
                200: {
                  description: "The list of users",
                  content: {
                    "application/json": {
                      schema: {
                        type: "array",
                        items: {
                          $ref: "#/components/schemas/User",
                        },
                      },
                    },
                  },
                },
                500: {
                  description: "Error fetching users",
                },
              },
            },
            post: {
              summary: "Creates a new user",
              tags: ["Users"],
              requestBody: {
                required: true,
                content: {
                  "application/json": {
                    schema: {
                      $ref: "#/components/schemas/User",
                    },
                  },
                },
              },
              responses: {
                201: {
                  description: "The created user",
                  content: {
                    "application/json": {
                      schema: {
                        $ref: "#/components/schemas/User",
                      },
                    },
                  },
                },
                500: {
                  description: "Error creating user",
                },
              },
            },
          },
          "/api/users/{id}": {
            get: {
              summary: "Gets a list of users",
              tags: ["Users"],
              parameters: [
                {
                  in: "path",
                  name: "id",
                  schema: {
                    type: "string",
                  },
                  required: true,
                  description: "The user id",
                },
              ],
              responses: {
                200: {
                  description: "The user",
                  content: {
                    "application/json": {
                      schema: {
                        $ref: "#/components/schemas/User",
                      },
                    },
                  },
                },
                404: {
                  description: "User not found",
                },
                500: {
                  description: "Error fetching user",
                },
              },
            },
            put: {
              summary: "Updates an existing user",
              tags: ["Users"],
              parameters: [
                {
                  in: "path",
                  name: "id",
                  schema: {
                    type: "string",
                  },
                  required: true,
                  description: "The user id",
                },
              ],
              requestBody: {
                required: true,
                content: {
                  "application/json": {
                    schema: {
                      $ref: "#/components/schemas/User",
                    },
                  },
                },
              },
              responses: {
                200: {
                  description: "The updated user",
                  content: {
                    "application/json": {
                      schema: {
                        $ref: "#/components/schemas/User",
                      },
                    },
                  },
                },
                404: {
                  description: "User not found",
                },
                500: {
                  description: "Error updating user",
                },
              },
            },
            delete: {
              summary: "Deletes an existing user",
              tags: ["Users"],
              parameters: [
                {
                  in: "path",
                  name: "id",
                  schema: {
                    type: "string",
                  },
                  required: true,
                  description: "The user id",
                },
              ],
              responses: {
                200: {
                  description: "User deleted successfully",
                },
                404: {
                  description: "User not found",
                },
                500: {
                  description: "Error deleting user",
                },
              },
            },
          },
          "/api/registerUser": {
            post: {
              summary: "Creates a new user with admin role",
              tags: ["Users"],
              requestBody: {
                required: true,
                content: {
                  "application/json": {
                    schema: {
                      type: "object",
                      properties: {
                        email: {
                          type: "string",
                          description: "Email address",
                        },
                        password: {
                          type: "string",
                          description: "Password",
                        },
                        token: {
                          type: "string",
                          description: "Token for authorization",
                        },
                      },
                      required: ["token"],
                    },
                  },
                },
              },
              responses: {
                201: {
                  description: "Successfully created user",
                  content: {
                    "application/json": {
                      schema: {
                        $ref: "#/components/schemas/User",
                      },
                    },
                  },
                },
                401: {
                  description: "Unauthorized",
                },
                500: {
                  description: "Error creating user",
                  content: {
                    "application/json": {
                      schema: {
                        type: "object",
                        properties: {
                          error: {
                            type: "string",
                          },
                        },
                      },
                    },
                  },
                },
              },
            },
          },
          "/api/bulkLeads": {
            post: {
              summary: "Upload bulk leads",
              tags: ["Leads"],
              requestBody: {
                required: true,
                content: {
                  "application/json": {
                    schema: {
                      type: "array",
                      items: {
                        $ref: "#/components/schemas/Lead",
                      },
                    },
                  },
                },
              },
              responses: {
                200: {
                  description: "Successfully created leads",
                },
                400: {
                  description: "Invalid request body",
                },
              },
            },
          },
          "/api/leadColumnsInfo": {
            get: {
              summary: "Gets information about lead columns",
              tags: ["LeadColumnsInfo"],
              responses: {
                200: {
                  description: "The lead columns information",
                  content: {
                    "application/json": {
                      schema: {
                        type: "array",
                        items: {
                          $ref: "#/components/schemas/LeadColumnInfo",
                        },
                      },
                    },
                  },
                },
                500: {
                  description: "Error fetching lead columns information",
                },
              },
            },
            post: {
              summary: "Creates lead columns information",
              tags: ["LeadColumnsInfo"],
              requestBody: {
                required: true,
                content: {
                  "application/json": {
                    schema: {
                      $ref: "#/components/schemas/LeadColumnInfo",
                    },
                  },
                },
              },
              responses: {
                201: {
                  description: "Successfully created lead columns information",
                  content: {
                    "application/json": {
                      schema: {
                        $ref: "#/components/schemas/LeadColumnInfo",
                      },
                    },
                  },
                },
                500: {
                  description: "Error creating lead columns information",
                },
              },
            },
          },
          "/api/leadColumnsInfo/{id}": {
            get: {
              summary: "Gets a specific lead column info by ID",
              tags: ["LeadColumnsInfo"],
              parameters: [
                {
                  in: "path",
                  name: "id",
                  schema: {
                    type: "string",
                  },
                  required: true,
                  description: "The lead column info id",
                },
              ],
              responses: {
                200: {
                  description: "The lead column info",
                  content: {
                    "application/json": {
                      schema: {
                        $ref: "#/components/schemas/LeadColumnInfo",
                      },
                    },
                  },
                },
                404: {
                  description: "Lead column info not found",
                },
                500: {
                  description: "Error fetching lead column info",
                },
              },
            },
            put: {
              summary: "Updates an existing lead column info",
              tags: ["LeadColumnsInfo"],
              parameters: [
                {
                  in: "path",
                  name: "id",
                  schema: {
                    type: "string",
                  },
                  required: true,
                  description: "The lead column info id",
                },
              ],
              requestBody: {
                required: true,
                content: {
                  "application/json": {
                    schema: {
                      $ref: "#/components/schemas/LeadColumnInfo",
                    },
                  },
                },
              },
              responses: {
                200: {
                  description: "The updated lead column info",
                  content: {
                    "application/json": {
                      schema: {
                        $ref: "#/components/schemas/LeadColumnInfo",
                      },
                    },
                  },
                },
                404: {
                  description: "Lead column info not found",
                },
                500: {
                  description: "Error updating lead column info",
                },
              },
            },
            delete: {
              summary: "Deletes an existing lead column info",
              tags: ["LeadColumnsInfo"],
              parameters: [
                {
                  in: "path",
                  name: "id",
                  schema: {
                    type: "string",
                  },
                  required: true,
                  description: "The lead column info id",
                },
              ],
              responses: {
                200: {
                  description: "Lead column info deleted successfully",
                },
                404: {
                  description: "Lead column info not found",
                },
                500: {
                  description: "Error deleting lead column info",
                },
              },
            },
          },
        },
        components: {
          schemas: {
            Account: {
              type: "object",
              properties: {
                name: { type: "string" },
                user: { type: "string" },
                status: {
                  type: "string",
                  enum: Object.values(AccountStatusEnum),
                },
              },
              required: ["name", "user", "status"],
            },
            Lead: {
              type: "object",
              properties: {
                avatar: {
                  type: "string",
                  description: "The avatar URL of the lead",
                },
                name: { type: "string", description: "The name of the lead" },
                role: { type: "string", description: "The role of the lead" },
                url: {
                  type: "string",
                  description: "The URL associated with the lead",
                },
                country: {
                  type: "string",
                  description: "The country of the lead",
                },
                category: {
                  type: "string",
                  description: "The category of the lead",
                },
                company: {
                  type: "string",
                  description: "The company of the lead",
                },
                location: {
                  type: "string",
                  description: "The location of the lead",
                },
                industry: {
                  type: "string",
                  description: "The industry of the lead",
                },
                note: {
                  type: "string",
                  description: "Additional notes about the lead",
                },
                tags: {
                  type: "array",
                  items: { type: "string" },
                  description: "Tags associated with the lead",
                },
                resource: {
                  type: "string",
                  description: "The resource from which the lead was obtained",
                },
                meta_data: {
                  type: "object",
                  additionalProperties: true,
                  description: "Metadata associated with the lead",
                },
                assigned_user: {
                  type: "string",
                  description: "The user to whom the lead is assigned",
                },
                assigned_account: {
                  type: "string",
                  description: "The account to which the lead is assigned",
                },
                action_status: {
                  type: "string",
                  enum: Object.values(ActionStatusEnum),
                  description: "The action status of the lead",
                },
                accept_status: {
                  type: "string",
                  enum: Object.values(AcceptStatusEnum),
                  description: "The acceptance status of the lead",
                },
              },
            },
            User: {
              type: "object",
              properties: {
                name: {
                  type: "string",
                  description: "The name of the user",
                },
                email: {
                  type: "string",
                  description: "The email of the user",
                },
                password: {
                  type: "string",
                  description: "The password of the user",
                },
              },
            },
            LeadColumnInfo: {
              type: "object",
              properties: {
                userId: {
                  type: "string",
                  description:
                    "The ID of the user associated with the lead column info",
                },
                leadColumnInfo: {
                  type: "array",
                  items: {
                    type: "object",
                    properties: {
                      title: {
                        type: "string",
                        description: "The title of the column",
                      },
                      checked: {
                        type: "boolean",
                        description: "Checked status of the column",
                      },
                    },
                    required: ["title", "checked"],
                  },
                },
              },
              required: ["userId", "leadColumnInfo"],
            },
          },
        },
      },
    });

    console.log("Generated spec:", spec);

    return {
      props: {
        spec,
      },
    };
  } catch (error) {
    console.error("Error generating spec:", error);
    return {
      props: {
        spec: {},
      },
    };
  }
};

export default ApiDoc;
