import LeadTable from '@/components/LeadTable';
import {
  Dropdown,
  MenuProps,
  Space,
  message
} from 'antd';
import Head from 'next/head';
import React, {
  useCallback,
  useContext,
  useEffect,
  useRef,
  useState
} from 'react';
import { debounce } from 'lodash';
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown';
import FileUpload from '@/components/BulkUpload';
import LeadForm from '@/components/LeadForm';
import { GetServerSidePropsContext } from 'next';
import { getSession } from 'next-auth/react';
import { BulkAssignedModal } from '@/components/BulkActions/BulkAssignedModal';
import StickyPagination from '@/components/StickyPagination';
import { useSelector, useDispatch } from 'react-redux';
import { selectAllAccounts } from '@/redux/selectors/accountsSelectors';
import {
  selectCountryOptions,
  selectUserOptions,
  selectCategoryOptions
} from '@/redux/selectors/optionsSelectors';
import { RootState } from '@/redux/reducers';
import { fetchAllOptions } from "@/redux/actions/optionsActions";
import { ClientContext } from '@/context/ClientContext';
import { BulkActionStatusModal } from '@/components/BulkActions/BulkActionStatusModal';
import { ItemType } from 'antd/es/menu/interface';
import { selectColumnInfo } from '@/redux/selectors/leadsSelectors';
import { Action, ThunkDispatch } from '@reduxjs/toolkit';
import ColumnSelector, { IColumnData } from '@/components/ColumnSelector';

export async function getServerSideProps(context: GetServerSidePropsContext) {
  const session = await getSession(context);
  return {
    props: { user: session?.user }
  };
}

export const INITIAL_PAGINATION = {
  current: 1,
  total: 0,
  pageSize: 10,
  showSizeChanger: true,
  pageSizeOptions: ['10', '15', '20', '25', '30', '50'],
  startIndex: 0,
  endIndex: 0,
  totalRecordsCount: 0,
};

const BULK_ACTION_MENU = {
  updateAction: 'update_action',
  assignAccount: 'assign_account',
  export: 'export'
};

type UserMenuOption = {
  key: string
  label: string
}

function Leads({
  user
}: {
  user: { id: string; email: string; role: 'user' | 'admin'; name: string };
}) {
  const { storedLeadColumns, setStoredLeadColumns, columnsArray } =
    useContext(ClientContext);
  const [isModalVisible, setModalVisible] = useState(false);
  const [isSettingsModalVisible, setIsSettingsModalVisible] = useState(false);
  const actionsStatus: MenuProps['items'] = [
    {
      label: 'Open',
      key: 'open'
    },
    {
      label: 'Sent',
      key: 'sent'
    },
    {
      label: 'Failed',
      key: 'failed'
    },
    {
      label: 'Archived',
      key: 'archived'
    }
  ];
  const [dataSource, setDataSource] = useState<Lead[]>([]);
  const [countries, setCountries] = useState<MenuProps['items']>([]);
  const [bulkActionMenu, setBulkActionMenu] = useState<MenuProps['items']>([]);
  const [categories, setCategories] = useState<MenuProps['items']>([]);
  const [assignedUsers, setAssignedUsers] = useState<UserMenuOption[]>([]);
  const [assignedAccounts, setAssignedAccounts] = useState<UserMenuOption[]>(
    []
  );
  const [filteredData, setFilteredData] = useState<Lead[]>([]);
  const [selectedRows, setSelectedRows] = useState<string[]>([]);
  const [filters, setFilters] = useState<{ [key: string]: string | undefined }>(
    {
      country: undefined,
      category: undefined,
      assignedUser: undefined,
      actionStatus: undefined
    }
  );
  const [assignedAccountModal, setAssignedAccountModal] = useState(false);
  const [bulkUpdateActionModal, setBulkUpdateActionModal] = useState(false);
  const [searchText, setSearchText] = useState('');
  const prevSearchTextRef = useRef<any>();
  const [pagination, setPagination] = useState(INITIAL_PAGINATION);
  const [isLoading, setIsLoading] = useState(false);
  const [isSaving, setIsSaving] = useState(false);
  const accounts = useSelector(selectAllAccounts);
  const countryOptions = useSelector(selectCountryOptions);
  const userOptions = useSelector(selectUserOptions);
  const categoryOptions = useSelector(selectCategoryOptions);
  const [accountLabel, setAccountLabel] = useState('');
  const [userLabel, setUserLabel] = useState('');
  const leadColumnInfo = useSelector(selectColumnInfo);

  const { allOptionsStatus } =
    useSelector((state: RootState) => state.options);

  const dispatch = useDispatch<ThunkDispatch<RootState, any, Action>>();
  const [leadColumns, setLeadColumns] = useState([] as IColumnData[])

  const handleMenuClick = (
    value: string | undefined,
    filterKey: string,
    v?: any
  ) => {
    const updatedFilters = { ...filters, [filterKey]: value };
    if ('assigned_account' === filterKey) {
      const selectedItem = assignedAccounts?.find(
        (item) => item?.key === value
      );
      if (selectedItem) {
        setAccountLabel(selectedItem.label as string);
      }
    }
    if ('assigned_user' === filterKey) {
      const selectedItem = assignedUsers?.find((item) => item?.key === value);
      if (selectedItem) {
        setUserLabel(selectedItem.label as string);
      }
    }
    setFilters(updatedFilters);
  };

  const handleBulkMenuClick = (key: string) => {
    if (key === BULK_ACTION_MENU.assignAccount) setAssignedAccountModal(true);
    if (key === BULK_ACTION_MENU.updateAction) setBulkUpdateActionModal(true);
    if (key === BULK_ACTION_MENU.export) handleExportData();
  };

  const clearFilters = () => {
    setFilters({
      country: undefined,
      category: undefined,
      assignedUser: undefined,
      actionStatus: undefined
    });
    setFilteredData(dataSource);
    setAccountLabel('');
    setUserLabel('');
  };

  const openModal = () => {
    setModalVisible(true);
  };

  const closeModal = () => {
    fetchData();
    setModalVisible(false);
  };

  const handleCancel = () => {
    setAssignedAccountModal(false);
    setBulkUpdateActionModal(false);
  };

  const fetchFilterOptions = async () => {
    if (user?.role === 'user') {
      const accountsOptions = accounts.map((item: any) => ({
        label: item.name,
        key: item._id
      }));
      setAssignedAccounts(accountsOptions);
    } else if (user?.role === 'admin') {
      const usersWithUnAssignedOption: any = [
        ...userOptions,
        { name: 'Unassigned', _id: '' }
      ];

      setCountries(
        countryOptions.map((country: string) => {
          return {
            label: country,
            key: country
          };
        })
      );
      setCategories(
        categoryOptions.map((category: string) => {
          return {
            label: category,
            key: category
          };
        })
      );
      setAssignedUsers(
        usersWithUnAssignedOption.map((user: { name: any; _id: any }) => {
          if (user) {
            return {
              label: user.name,
              key: user._id
            };
          }
        })
      );
    }
  };

  const fetchData = async () => {
    setIsLoading(true);
    let url = `/api/leads?page=${pagination.current}&limit=${pagination.pageSize}`;
    for (const key in filters) {
      if (filters[key] !== undefined) {
        url += `&${key}=${filters[key]}`;
      }
    }
    if (searchText) {
      url += `&search=${searchText}`;
    }
    const leadsRes = await fetch(url);
    const leadsData = await leadsRes.json();
    setIsLoading(false);
    setDataSource(leadsData.leads);
    setPagination({
      ...pagination,
      total: leadsData.count,
      startIndex: leadsData.startIndex,
      endIndex: leadsData.endIndex,
      totalRecordsCount: leadsData.totalRecords,
    });
    setFilteredData(leadsData.leads);
  };

  const handleColumnUpdate = async (items: IColumnData[]) => {
    const payload = items.map((item) => ({
      _id: item._id,
      title: item.title,
      checked: item.checked,
    }));
    setIsSaving(true)
    const res = await fetch("/api/lead-column-info", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        userId: user.id,
        leadColumnInfo: payload,
      }),
    });
    res.json().then((data) => {
      setIsSaving(false)
      const updatedCols = data.leadColumnInfo.map((item: { _id: string }) => ({
        ...item,
        id: item._id,
      }))
      setLeadColumns(updatedCols)
      setStoredLeadColumns(updatedCols)
      setIsSettingsModalVisible(false)
    });
  }

  useEffect(() => {
    const menu = [
      { label: 'Update action', key: BULK_ACTION_MENU.updateAction },
      { label: 'Export selected', key: BULK_ACTION_MENU.export }
    ];
    if (user?.role === 'admin') {
      menu.push({
        label: 'Assign account',
        key: BULK_ACTION_MENU.assignAccount
      });
    }
    setBulkActionMenu(menu);
  }, []);

  useEffect(() => {
    if (allOptionsStatus === 'idle') dispatch(fetchAllOptions());

    fetchFilterOptions();
  }, [dispatch, allOptionsStatus]);

  useEffect(() => {
    fetchData();
  }, [pagination.current, pagination.pageSize, filters]);

  useEffect(() => {
    if (prevSearchTextRef.current && searchText === '') {
      fetchData();
    }
    prevSearchTextRef.current = searchText;
  }, [searchText]);

  const handleExportData = () => {
    const rows = dataSource.filter((data: Lead) =>
      selectedRows.includes(data._id)
    );
    const headers = [
      'Name',
      'Role',
      'URL',
      'Company',
      'Location',
      'Generated Date'
    ];
    const keys = ['name', 'role', 'url', 'company', 'location', 'createdAt'];
    const csvRows = [];
    csvRows.push(headers.join(','));

    for (const row of rows) {
      const values = keys.map((key) => {
        // @ts-ignore
        const escaped = ('' + row[key]).replace(/"/g, '\\"');
        return `"${escaped}"`;
      });
      csvRows.push(values.join(','));
    }

    const csvString = csvRows.join('\n');
    const blob = new Blob([csvString], { type: 'text/csv' });
    const url = window.URL.createObjectURL(blob);
    const a = document.createElement('a');
    a.setAttribute('hidden', '');
    a.setAttribute('href', url);
    a.setAttribute('download', 'leads.csv');
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
  };

  const handleBulkAssignedUpdate = async (data: {
    assignedUser: string;
    assignedAccount: string;
  }) => {
    try {
      const payload = {
        ids: selectedRows,
        assigned_user: data.assignedUser,
        assigned_account: data.assignedAccount
      };

      const res = await fetch('/api/update-leads', {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(payload)
      });

      const resData = await res.json();

      if (!resData.error) {
        message.success(resData.message);
        fetchData();
        setSelectedRows([]);
        setAssignedAccountModal(false);
      } else {
        message.error('Error updating leads');
      }
    } catch (err) {
      message.error('Error updating leads');
    }
  };

  const handleBulkActionStatusUpdate = async ({
    actionStatus
  }: {
    actionStatus: string;
  }) => {
    const payload = {
      ids: selectedRows,
      action_status: actionStatus
    };
    const res = await fetch('/api/update-leads', {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(payload)
    });
    const resData = await res.json();
    if (resData) {
      fetchData();
      setSelectedRows([]);
      setBulkUpdateActionModal(false);
    }
  };

  const clearSearch = async () => {
    setSearchText('');
    setPagination({ ...pagination, current: 1 });
  };

  const handleSearch = async () => {
    setIsLoading(true);
    setPagination((prev: any) => ({ ...prev, current: 1 }));
    setFilters({
      country: undefined,
      category: undefined,
      assignedUser: undefined,
      actionStatus: undefined
    });
    setAccountLabel('');
    setUserLabel('');
    await fetchData();
    setIsLoading(false);
  };

  const debouncedSearch = useCallback(
    debounce(() => {
      handleSearch();
    }, 500),
    [
      pagination.pageSize,
      setPagination,
      setDataSource,
      setFilteredData,
      setIsLoading
    ]
  );

  const handleInputChange = async (e: any) => {
    setSearchText(e.target.value);
    debouncedSearch();
  };

  const removeFilterValues = (e: any, key: string) => {
    e.stopPropagation();
    const updatedFilter = { ...filters, [key]: undefined };
    if (key === 'assigned_user') setUserLabel('');
    if (key === 'assigned_account') setAccountLabel('');
    setFilters(updatedFilter);
  };

  const fetchLeadColumnInfo = async () => {
    const res = await fetch(`/api/lead-column-info?userId=${user.id}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      }
    });
    res.json().then((data) => {
      if (data.length > 0) {
        const columns = data[0].leadColumnInfo.map((item: { _id: string; }) => ({...item, id: item._id}))
        setLeadColumns(columns)
        setStoredLeadColumns(columns);
      } else {
        handleColumnUpdate(columnsArray)
      }
    });
  };
  useEffect(() => {
    if (leadColumnInfo.length > 0) {
      setStoredLeadColumns(leadColumnInfo[0].leadColumnInfo)
      const columns = leadColumnInfo[0].leadColumnInfo.map((item: { _id: string; }) => ({...item, id: item._id}))
      setLeadColumns(columns)
    } else {
      fetchLeadColumnInfo()
    }
  }, []);

  return (
    <>
      <Head>
        <title>Leads | Event Management Application</title>
      </Head>
      <LeadForm
        user={user}
        isVisible={isModalVisible}
        onClose={closeModal}
        type="add"
      />
      <ColumnSelector
        isSaving={isSaving}
        isSettingsModalVisible={isSettingsModalVisible}
        setIsSettingsModalVisible={setIsSettingsModalVisible}
        columns={leadColumns}
        handleUpdate={handleColumnUpdate}
      />
      <div className="p-5 bg-white">
        <div className="flex justify-between items-center">
          <div className="flex gap-3 items-center">
            <h1 className="text-lg font-bold text-gray-700">Leads</h1>
          </div>
          <div className="flex gap-3 items-center w-full justify-end">
            <div className="flex items-center bg-[#F9F9F5] gap-2 px-3 py-2 rounded-lg ml-1 w-full max-w-[350px] relative">
              <i className="fa-solid fa-magnifying-glass text-gray-500 text-base bg-inherit"></i>
              <input
                type="search"
                name="search"
                id="header-search"
                placeholder="Search"
                value={searchText}
                onChange={handleInputChange}
                className="bg-inherit focus:outline-none text-base w-full"
              />
              {searchText && (
                <button
                  onClick={clearSearch}
                  className="absolute top-1/2 right-3 transform -translate-y-1/2 text-gray-500 hover:text-gray-700 cursor-pointer opacity-0"
                >
                  &#x2715;
                </button>
              )}
            </div>
            {user?.role !== "user" && <FileUpload />}
            {user?.role === "admin" && (
              <button
                onClick={openModal}
                className="flex gap-2 items-center bg-[#F9D300] hover:bg-[#c7af26] rounded-lg px-3 py-2 text-base font-normal text-gray-700"
              >
                <i className="fa-solid fa-plus"></i>
                <span>Add lead</span>
              </button>
            )}
            {selectedRows.length > 0 && user?.role === "admin" && (
              <button
                className="flex gap-2 items-center bg-[#F9D300] hover:bg-[#c7af26] rounded-lg px-3 py-2 text-base font-normal text-gray-700"
                onClick={handleExportData}
              >
                <i className="fa-solid fa-file-csv"></i>
                <span>Export Selected</span>
              </button>
            )}
            {selectedRows.length > 0 && (
              <Dropdown
                className="hover:cursor-pointer"
                menu={{
                  items: bulkActionMenu,
                  onClick: ({ key }: { key: string }) =>
                    handleBulkMenuClick(key),
                }}
                trigger={["click"]}
              >
                <a
                  className="border rounded-lg px-3 py-[7px]"
                  onClick={(e) => e.preventDefault()}
                >
                  <Space>
                    Bulk actions
                    <ArrowDropDownIcon className="!h-5" />
                  </Space>
                </a>
              </Dropdown>
            )}
          </div>
        </div>
        <div className="mt-5 flex justify-between items-center">
          <div className="text-base antd-fonts">
            {selectedRows.length > 0 ? (
              <p>{selectedRows.length} leads selected</p>
            ) : (
              <p>
                Showing {pagination.total === 0 ? 0 : pagination.startIndex} to {pagination.total === 0 ? 0 : pagination.endIndex} of{" "}
                {pagination.totalRecordsCount}
              </p>
            )}
          </div>
          <div className="flex justify-end items-center gap-3">
            {user?.role === "admin" && (
              <>
                <Dropdown
                  className="hover:cursor-pointer"
                  menu={{
                    items: countries,
                    onClick: ({ key }: { key: string }) =>
                      handleMenuClick(key, "country"),
                  }}
                  trigger={["click"]}
                >
                  <a
                    className={`border rounded-lg px-3 py-1 h-[40px] flex items-center ${
                      filters.country ? "bg-yellow-100" : ""
                    } `}
                    onClick={(e) => e.preventDefault()}
                  >
                    <Space>
                      Country{" "}
                      <span className="capitalize">{filters.country}</span>
                      {filters.country ? (
                        <span
                          onClick={(e: any) => removeFilterValues(e, "country")}
                        >
                          <i className="fas fa-times"></i>
                        </span>
                      ) : (
                        <ArrowDropDownIcon />
                      )}
                    </Space>
                  </a>
                </Dropdown>
                <Dropdown
                  className="hover:cursor-pointer"
                  menu={{
                    items: categories,
                    onClick: ({ key }: { key: string }) =>
                      handleMenuClick(key, "category"),
                  }}
                  trigger={["click"]}
                >
                  <a
                    className={`border rounded-lg px-3 py-1 h-[40px] flex items-center ${
                      filters.category ? "bg-yellow-100" : ""
                    } `}
                    onClick={(e) => e.preventDefault()}
                  >
                    <Space>
                      Category{" "}
                      <span className="capitalize">{filters.category}</span>
                      {filters.category ? (
                        <span
                          onClick={(e: any) =>
                            removeFilterValues(e, "category")
                          }
                        >
                          <i className="fas fa-times"></i>
                        </span>
                      ) : (
                        <ArrowDropDownIcon />
                      )}
                    </Space>
                  </a>
                </Dropdown>
                <Dropdown
                  className="hover:cursor-pointer"
                  menu={{
                    items: assignedUsers,
                    onClick: ({ key, ...v }: { key: string }) =>
                      handleMenuClick(key, "assigned_user", v),
                  }}
                  trigger={["click"]}
                >
                  <a
                    className={`border rounded-lg px-3 py-1 h-[40px] flex items-center ${
                      filters.assigned_user !== undefined ? "bg-yellow-100" : ""
                    } `}
                    onClick={(e) => e.preventDefault()}
                  >
                    <Space>
                      Assigned User{" "}
                      <span className="capitalize">{userLabel}</span>
                      {filters.assigned_user !== undefined ? (
                        <span
                          onClick={(e: any) =>
                            removeFilterValues(e, "assigned_user")
                          }
                        >
                          <i className="fas fa-times"></i>
                        </span>
                      ) : (
                        <ArrowDropDownIcon />
                      )}
                    </Space>
                  </a>
                </Dropdown>
              </>
            )}
            {user?.role === "user" && (
              <Dropdown
                className="hover:cursor-pointer"
                menu={{
                  items: assignedAccounts,
                  onClick: ({ key }: { key: string }) =>
                    handleMenuClick(key, "assigned_account"),
                }}
                trigger={["click"]}
              >
                <a
                  className={`border rounded-lg px-3 py-1 h-[40px] flex items-center ${
                    filters.assigned_account ? "bg-yellow-100" : ""
                  } `}
                  onClick={(e) => e.preventDefault()}
                >
                  <Space>
                    Assigned Account{" "}
                    <span className="capitalize">{accountLabel}</span>
                    {filters.assigned_account ? (
                      <span
                        onClick={(e: any) =>
                          removeFilterValues(e, "assigned_account")
                        }
                      >
                        <i className="fas fa-times"></i>
                      </span>
                    ) : (
                      <ArrowDropDownIcon />
                    )}
                  </Space>
                </a>
              </Dropdown>
            )}
            <Dropdown
              className="hover:cursor-pointer"
              menu={{
                items: actionsStatus,
                onClick: ({ key }: { key: string }) =>
                  handleMenuClick(key, "action_status"),
              }}
              trigger={["click"]}
            >
              <a
                className={`border rounded-lg px-3 py-1 h-[40px] flex items-center ${
                  filters.action_status ? "bg-yellow-100" : ""
                } `}
                onClick={(e) => e.preventDefault()}
              >
                <Space>
                  Action Status{" "}
                  <span className="capitalize">{filters.action_status}</span>
                  {filters.action_status ? (
                    <span
                      onClick={(e: any) =>
                        removeFilterValues(e, "action_status")
                      }
                    >
                      <i className="fas fa-times"></i>
                    </span>
                  ) : (
                    <ArrowDropDownIcon />
                  )}
                </Space>
              </a>
            </Dropdown>
          </div>
        </div>
      </div>
      <div className="px-5 w-full h-full">
        <LeadTable
          isLoading={isLoading}
          user={user}
          data={filteredData}
          updateSelectedRows={setSelectedRows}
          selectedRowKeys={selectedRows}
          fetchData={fetchData}
          isVisibleModal={isSettingsModalVisible}
          setIsVisibleModal={setIsSettingsModalVisible}
        />
      </div>
      {assignedAccountModal ? (
        <BulkAssignedModal
          bulkModalOpen={assignedAccountModal}
          handleCancel={handleCancel}
          handleBulkUpdate={handleBulkAssignedUpdate}
        />
      ) : null}

      {bulkUpdateActionModal ? (
        <BulkActionStatusModal
          bulkModalOpen={bulkUpdateActionModal}
          handleCancel={handleCancel}
          handleBulkUpdate={handleBulkActionStatusUpdate}
        />
      ) : null}
      <StickyPagination pagination={pagination} setPagination={setPagination} />
    </>
  );
}

export default Leads;
