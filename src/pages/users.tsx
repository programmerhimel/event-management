import RightSidebarModal from "@/components/RightSidebarModal";
import UsersTable from "@/components/UsersTable";
import { ClientContext } from "@/context/ClientContext";
import { Select } from "antd";
import { GetServerSidePropsContext } from "next";
import { getSession } from "next-auth/react";
import Head from "next/head";
import { useRouter } from "next/router";
import React, { useContext, useState, useEffect } from "react";
import Loader from "@/components/Loader";
import { EyeOutlined, EyeInvisibleOutlined } from "@ant-design/icons";
import { useDispatch, useSelector } from "react-redux";
import {
  selectAllUsers,
  selectUsersStatus,
} from "@/redux/selectors/usersSelectors";
import { fetchUsers } from "@/redux/actions/usersActions";
import { Action, AsyncThunkAction, ThunkDispatch } from "@reduxjs/toolkit";
import { RootState } from "@/redux/reducers";
import StickyPagination from "@/components/StickyPagination";
import { INITIAL_PAGINATION } from "./leads";

export async function getServerSideProps(context: GetServerSidePropsContext) {
  const session = await getSession(context);
  if (session?.user?.role === "user") {
    return {
      redirect: {
        destination: "/leads",
        permanent: false,
      },
    };
  }
  return {
    props: { user: session?.user },
  };
}

const Users = () => {
  const { isEditMode, setIsEditMode, editUserData } = useContext(ClientContext);
  const [isModalVisible, setModalVisible] = useState(false);
  const [isPasswordVisible, setIsPasswordVisible] = useState(false);
  const [pagination, setPagination] = useState(INITIAL_PAGINATION);
  const [isAdding, setIsAdding] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [role, setRole] = useState("");
  const router = useRouter();
  const usersStatus = useSelector(selectUsersStatus);
  const users = useSelector(selectAllUsers);
  const dispatch = useDispatch<ThunkDispatch<RootState, any, Action>>();

  useEffect(() => {
    if (usersStatus === "loading") {
      setIsLoading(true);
    } else if (usersStatus === "succeeded") {
      setTimeout(() => {
        setIsLoading(false);
      }, 300);
    }
  }, [usersStatus]);

  const openModal = () => {
    setModalVisible(true);
  };

  const closeModal = () => {
    setIsEditMode(false);
    setName("");
    setEmail("");
    setPassword("");
    setRole("");
    setModalVisible(false);
  };

  const handleChange = (value: string) => {
    setRole(value);
  };

  const handleSubmit = async (e: React.FormEvent) => {
    e.preventDefault();
    setIsAdding(true);

    const res = await fetch("/api/auth/register", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ name, email, password, role }),
    });

    if (res.ok) {
      setIsAdding(false);
      closeModal();
      dispatch(fetchUsers());
    } else {
      console.error(await res.json());
    }
  };

  const handleEditUser = async (e: React.FormEvent) => {
    e.preventDefault();
    setIsAdding(true);

    const userData = {
      name: name === "" ? editUserData.name : name,
      email: email === "" ? editUserData.email : email,
      role:
        role === "" || role === editUserData.role ? editUserData.role : role,
    };

    const res = await fetch(`/api/users?id=${editUserData._id}`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(userData),
    });

    if (res.ok) {
      setIsAdding(false);
      closeModal();
      dispatch(fetchUsers());
    } else {
      console.error(await res.json());
    }
  };

  return (
    <>
      <Head>
        <title>Users | Event Management Application</title>
      </Head>
      <RightSidebarModal
        isVisible={isEditMode ? isEditMode : isModalVisible}
        onClose={closeModal}
      >
        <form
          onSubmit={isEditMode ? handleEditUser : handleSubmit}
          className="w-full h-full flex flex-col justify-between items-center"
        >
          <div className="p-[30px] w-full">
            <h2 className="mb-10 text-center text-2xl font-bold leading-9 tracking-tight text-gray-900">
              {isEditMode ? "Edit user" : "Add a user"}
            </h2>
            <div className="space-y-6">
              <div>
                <label
                  htmlFor="fullname"
                  className="block text-sm font-medium leading-6 text-gray-900"
                >
                  Name
                </label>
                <div className="mt-2">
                  {isEditMode ? (
                    <input
                      id="fullname"
                      name="fullname"
                      type="text"
                      value={name === "" ? editUserData.name : name}
                      onChange={(e) => setName(e.target.value)}
                      required
                      className="block w-full rounded-md border-0 py-1.5 px-2 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                    />
                  ) : (
                    <input
                      id="fullname"
                      name="fullname"
                      type="text"
                      value={name}
                      onChange={(e) => setName(e.target.value)}
                      required
                      className="block w-full rounded-md border-0 py-1.5 px-2 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                    />
                  )}
                </div>
              </div>

              <div>
                <label
                  htmlFor="email"
                  className="block text-sm font-medium leading-6 text-gray-900"
                >
                  Email
                </label>
                <div className="mt-2">
                  {isEditMode ? (
                    <input
                      id="email"
                      name="email"
                      type="email"
                      autoComplete="email"
                      value={email === "" ? editUserData.email : email}
                      onChange={(e) => setEmail(e.target.value)}
                      required
                      className="block w-full rounded-md border-0 py-1.5 px-2 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                    />
                  ) : (
                    <input
                      id="email"
                      name="email"
                      type="email"
                      autoComplete="email"
                      value={email}
                      onChange={(e) => setEmail(e.target.value)}
                      required
                      className="block w-full rounded-md border-0 py-1.5 px-2 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                    />
                  )}
                </div>
              </div>

              {!isEditMode && (
                <div>
                  <div className="flex items-center justify-between">
                    <label
                      htmlFor="password"
                      className="block text-sm font-medium leading-6 text-gray-900"
                    >
                      Password
                    </label>
                  </div>
                  <div className="mt-2 flex w-full items-center gap-2 relative">
                    <input
                      id="password"
                      name="password"
                      type={`${isPasswordVisible ? "text" : "password"}`}
                      value={password}
                      onChange={(e) => setPassword(e.target.value)}
                      autoComplete="current-password"
                      required
                      className="block w-full rounded-md border-0 py-1.5 px-2 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                    />
                    <button
                      type="button"
                      onClick={() => setIsPasswordVisible(!isPasswordVisible)}
                      className="absolute inset-y-0 right-0 flex items-center px-2"
                    >
                      {isPasswordVisible ? (
                        <EyeInvisibleOutlined className="text-gray-500" />
                      ) : (
                        <EyeOutlined className="text-gray-500" />
                      )}
                    </button>
                  </div>
                </div>
              )}

              <div>
                <label className="block text-sm font-medium leading-6 text-gray-900 mb-2">
                  Role
                </label>
                {isEditMode ? (
                  <Select
                    value={role === "" ? editUserData.role : role}
                    style={{ width: "100%" }}
                    onChange={handleChange}
                    options={[
                      {
                        label: <span>Role</span>,
                        title: "manager",
                        options: [
                          { label: <span>Admin</span>, value: "admin" },
                          { label: <span>User</span>, value: "user" },
                        ],
                      },
                    ]}
                  />
                ) : (
                  <Select
                    value={role === "" ? "admin" : role}
                    style={{ width: "100%" }}
                    onChange={handleChange}
                    options={[
                      {
                        label: <span>Role</span>,
                        title: "manager",
                        options: [
                          { label: <span>Admin</span>, value: "admin" },
                          { label: <span>User</span>, value: "user" },
                        ],
                      },
                    ]}
                  />
                )}
              </div>
            </div>
          </div>
          <div className="w-full p-[30px] flex justify-end">
            <button
              type="submit"
              className="flex w-full max-w-[120px] justify-center items-center gap-1 rounded-md bg-indigo-600 px-3 py-1.5 text-sm font-semibold leading-6 text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
            >
              {isAdding && (
                <i className="fa-solid fa-circle-notch animate-spin"></i>
              )}
              {isEditMode ? "Update" : "Save"}
            </button>
          </div>
        </form>
      </RightSidebarModal>
      <div className="p-5 bg-white">
        <div className="flex justify-between items-center">
          <div className="flex gap-3 items-center">
            <h1 className="text-lg font-bold text-gray-700">Users</h1>
          </div>
          <button
            onClick={openModal}
            className="flex gap-2 items-center bg-[#F9D300] hover:bg-[#c7af26] rounded-lg px-3 py-2 text-base font-semibold text-gray-700"
          >
            <i className="fa-solid fa-plus"></i>
            <span>Add User</span>
          </button>
        </div>
      </div>
      <div className="px-5 w-full h-full">
        <UsersTable
          data={users as unknown as User[]}
          isLoading={usersStatus === 'loading'}
        />
      </div>
      <StickyPagination pagination={pagination} setPagination={setPagination} />
    </>
  );
};

export default Users;
