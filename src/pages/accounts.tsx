import AccountsTable from "@/components/AccountsTable";
import { MenuProps, Select, Dropdown, Space, message, Input } from "antd";
import Head from "next/head";
import React, { useState, useEffect, useContext } from "react";
import { useRouter } from "next/router";
import RightSidebarModal from "@/components/RightSidebarModal";
import ArrowDropDownIcon from "@mui/icons-material/ArrowDropDown";
import { ClientContext } from "@/context/ClientContext";
import moment from "moment";
import { getSession } from "next-auth/react";
import { GetServerSidePropsContext } from "next";
import { useDispatch, useSelector } from "react-redux";
import { Action, AsyncThunkAction, ThunkDispatch } from "@reduxjs/toolkit";
import { RootState } from "@/redux/reducers";
import { selectAccountsStatus, selectAllAccounts } from "@/redux/selectors/accountsSelectors";
import { selectAllUsers } from "@/redux/selectors/usersSelectors";
import { ItemType } from "antd/es/menu/interface";
import { fetchAccounts } from "@/redux/actions/accountsActions";
import StickyPagination from "@/components/StickyPagination";
import { INITIAL_PAGINATION } from "./leads";

export async function getServerSideProps(context: GetServerSidePropsContext) {
  const session = await getSession(context);
  return {
    props: { user: session?.user },
  };
}

type UserMenuOption = {
  key: string
  label: string
}

function Accounts({
  user,
}: {
  user: { id: string; email: string; role: "user" | "admin"; name: string };
}) {
  const { isEditMode, setIsEditMode, editAccountData } =
    useContext(ClientContext);
  const [isModalVisible, setModalVisible] = useState(false);
  const [isAdding, setIsAdding] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [pagination, setPagination] = useState(INITIAL_PAGINATION);
  const [accountUser, setAccountUser] = useState(
    editAccountData?.user?._id || ""
  );
  const [accountName, setAccountName] = useState(
    editAccountData?.user?.name || ""
  );
  const [accountStatus, setAccountStatus] = useState("active");
  const router = useRouter();
  const [usersOptions, setUsersOptions] = useState<UserMenuOption[]>([]);
  const [filteredData, setFilteredData] = useState<Account[]>([]);
  const [filters, setFilters] = useState<{ [key: string]: string | undefined }>(
    {
      user: undefined,
    }
  );
  const [userOptions, setUserOptions] = useState<
    { name: string; id: string }[]
  >([]);

  const [userLabel, setUserLabel] = useState('')
  const [errors, setErrors] = useState<any>();
  const [searchText, setSearchText] = useState("");
  const accountsStatus = useSelector(selectAccountsStatus);
  const accounts = useSelector(selectAllAccounts);
  const users = useSelector(selectAllUsers);
  const dispatch = useDispatch<ThunkDispatch<RootState, any, Action>>();

  const handleMenuClick = (value: string | undefined, filterKey: string) => {
    const updatedFilters = { ...filters, [filterKey]: value };
    setFilters(updatedFilters);

    if ('user' === (filterKey)) {
      const selectedItem = usersOptions?.find((item) => item?.key === value)
      if (selectedItem) {
        setUserLabel(selectedItem.label as string)
      }
    }
    let filtered: Account[] = accounts as any;

    if (updatedFilters.user) {
      filtered = filtered.filter((item: any) => item.user && item.user._id === updatedFilters.user);
    }

    setFilteredData(filtered);
  };

  const updateFilterData = () => {
    const formatedData = accounts.map((item: any) => ({
      ...item,
      userName: item.user?.name,
    }));
    const filteredBySearchText = formatedData.filter(item => item.name?.toString().toLowerCase().includes(searchText.toLowerCase()));
    setFilteredData(filteredBySearchText);
    const uniqueUsers = [
      ...new Set<string>(
        accounts.map((item: any) => item.user?._id)
      ),
    ];
    setUsersOptions(
      uniqueUsers.map((assignedUser) => {
        const userItem: any = users.find(
          (user: any) => user._id === assignedUser
        );
        if (userItem) {
          return {
            label: userItem.name,
            key: assignedUser,
          };
        } else {
          return {
            label: "Unknown User",
            key: assignedUser,
          };
        }
      })
    );
  };

  useEffect(() => {
    if (accountsStatus === 'loading') {
      setIsLoading(true);
    } else if (accountsStatus === 'succeeded') {
      setTimeout(() => {
        updateFilterData();
        setIsLoading(false);
      }, 300);
    }
  }, [accountsStatus, searchText]);

  useEffect(() => {
    if (!editAccountData) return;
    setAccountName(editAccountData.name);
    setAccountUser(editAccountData?.user?._id || "");
  }, [editAccountData]);

  const getUserOptions = () => {
    if (user?.role === "user") {
      setUserOptions([{ name: user.name, id: user?.id }]);
    } else {
      const options: any = users.map((usr) => ({
        name: usr.name,
        id: usr._id,
      }));
      setUserOptions(options);
    }
  };

  useEffect(() => {
    if (isEditMode) {
      getUserOptions();
    }
  }, [isEditMode]);

  const openModal = () => {
    setModalVisible(true);
    getUserOptions();
  };

  const closeModal = () => {
    setIsEditMode(false);
    setAccountName("");
    setAccountUser("");
    setAccountStatus("active");
    setModalVisible(false);
  };

  const handleChange = (value: string) => {
    setAccountStatus(value);
  };

  const clearSearch = () => {
    setSearchText("");
  };

  const handleSubmit = async (e: React.FormEvent) => {
    e.preventDefault();
    setIsAdding(true);

    const accountData = {
      name: accountName,
      user: accountUser,
      status: accountStatus,
    };

    const res = await fetch("/api/accounts", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(accountData),
    });

    if (res.ok) {
      setIsAdding(false);
      closeModal();
      dispatch(fetchAccounts());
    } else {
      console.error(await res.json());
    }
  };

  const handleEditAccount = async (e: React.FormEvent) => {
    e.preventDefault();
    setIsAdding(true);

    const accountData = {
      name: accountName === "" ? editAccountData.name : accountName,
      user: accountUser === "" ? editAccountData.user : accountUser,
      status:
        accountStatus === editAccountData.status
          ? editAccountData.status
          : accountStatus,
      updatedAt: moment().utc().toISOString(),
    };

    const res = await fetch(`/api/accounts?id=${editAccountData._id}`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(accountData),
    });

    if (res.ok) {
      setIsAdding(false);
      closeModal();
      dispatch(fetchAccounts());
    } else {
      console.error(await res.json());
    }
  };

  const validateForm = () => {
    const newErrors: any = {};
    if (!accountName) newErrors.name = "Name is required";
    if (!accountUser) newErrors.user = "User is required";
    if (!accountStatus) newErrors.status = "Status is required";
    setErrors(newErrors);
    return Object.keys(newErrors).length === 0;
  };

  const handleFormSubmit = (e: React.FormEvent) => {
    e.preventDefault();
    if (validateForm()) {
      if (isEditMode) {
        handleEditAccount(e);
      } else {
        handleSubmit(e);
      }
    } else {
      message.error("Please fill out all required fields.");
    }
  };

  const removeFilterValues = (e: any, key: string) => {
    e.stopPropagation()
    const updatedFilter = {...filters, [key]: ''}
    if (key === 'user') setUserLabel('')
    setFilters(updatedFilter)
    setFilteredData(accounts as any);
  }

  const allowEditingAllField = user?.role === 'admin' || !isEditMode

  return (
    <>
      <Head>
        <title>Accounts | Event Management Application</title>
      </Head>
      <RightSidebarModal
        isVisible={isEditMode ? isEditMode : isModalVisible}
        onClose={closeModal}
      >
        <form
          onSubmit={handleFormSubmit}
          className="w-full h-full flex flex-col justify-between items-center"
        >
          <div className="p-[30px] w-full">
            <h2 className="mb-10 text-center text-2xl font-bold leading-9 tracking-tight text-gray-900">
              {isEditMode ? "Edit account" : "Add an account"}
            </h2>
            <div className="space-y-6">
              <div>
                <label
                  htmlFor="fullname"
                  className="block text-sm font-medium leading-6 text-gray-900"
                >
                  Name
                </label>
                <div className="mt-2">
                  {
                    <input
                      id="name"
                      name="name"
                      type="text"
                      value={accountName}
                      onChange={(e) => setAccountName(e.target.value)}
                      required
                      className="block w-full rounded-md border-0 py-1.5 px-2 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                      readOnly={!allowEditingAllField}
                    />
                  }
                </div>
              </div>

              <div>
                <label
                  htmlFor="user"
                  className="block text-sm font-medium leading-6 text-gray-900"
                >
                  User
                </label>
                {allowEditingAllField ? (
                  <Select
                    style={{ width: "100%" }}
                    onChange={(value: string) => {
                      setAccountUser(value);
                    }}
                    value={accountUser}
                    options={[
                      {
                        label: <span>User</span>,
                        title: "manager",
                        options: userOptions.map((item) => ({
                          label: item.name,
                          value: item.id,
                        })),
                      },
                    ]}
                  />
                ) : (
                  <Input value={editAccountData.userName} readOnly />
                )}
              </div>

              <div>
                <label className="block text-sm font-medium leading-6 text-gray-900 mb-2">
                  Status
                </label>
                {isEditMode ? (
                  <Select
                    value={editAccountData.status}
                    style={{ width: "100%" }}
                    onChange={handleChange}
                    options={[
                      {
                        label: <span>Account Status</span>,
                        title: "manager",
                        options: [
                          { label: <span>Active</span>, value: "active" },
                          { label: <span>Pending</span>, value: "pending" },
                          {
                            label: <span>Restricted</span>,
                            value: "restricted",
                          },
                          { label: <span>Archived</span>, value: "archived" },
                          { label: <span>Assigned</span>, value: "assigned" },
                        ],
                      },
                    ]}
                  />
                ) : (
                  <Select
                    defaultValue={accountStatus}
                    style={{ width: "100%" }}
                    onChange={handleChange}
                    options={[
                      {
                        label: <span>Account Status</span>,
                        title: "manager",
                        options: [
                          { label: <span>Active</span>, value: "active" },
                          { label: <span>Pending</span>, value: "pending" },
                          {
                            label: <span>Restricted</span>,
                            value: "restricted",
                          },
                          { label: <span>Archived</span>, value: "archived" },
                          { label: <span>Assigned</span>, value: "assigned" },
                        ],
                      },
                    ]}
                  />
                )}
              </div>
            </div>
          </div>
          <div className="w-full p-[30px] flex justify-end">
            <button
              type="submit"
              className="flex w-full max-w-[120px] justify-center items-center gap-1 rounded-md bg-indigo-600 px-3 py-1.5 text-sm font-semibold leading-6 text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
            >
              {isAdding && (
                <i className="fa-solid fa-circle-notch animate-spin"></i>
              )}
              {isEditMode ? "Update" : "Save"}
            </button>
          </div>
        </form>
      </RightSidebarModal>
      <div className="p-5 bg-white">
        <div className="flex justify-between items-center">
          <div className="flex gap-3 items-center">
            <h1 className="text-lg font-bold text-gray-700">Accounts</h1>
          </div>
          <div className="flex gap-3 items-center w-full justify-end">
            <div className="flex items-center bg-[#F9F9F5] gap-2 px-3 py-2 rounded-lg ml-1 w-full max-w-[350px] h-[40px] relative">
              <i className="fa-solid fa-magnifying-glass text-gray-500 text-sm bg-inherit"></i>
              <input
                type="search"
                name="search"
                id="header-search"
                placeholder="Search"
                value={searchText}
                onChange={(e) => setSearchText(e.target.value)}
                className="bg-inherit focus:outline-none text-base w-full"
              />
              {searchText && (
                <button
                  onClick={clearSearch}
                  className="absolute top-1/2 right-3 transform -translate-y-1/2 text-gray-500 hover:text-gray-700 cursor-pointer opacity-0"
                >
                  &#x2715;
                </button>
              )}
            </div>
            {user?.role === "admin" && (
              <button
                onClick={openModal}
                className="flex gap-2 items-center bg-[#F9D300] hover:bg-[#c7af26] rounded-lg px-3 py-2 text-base font-semibold text-gray-700"
              >
                <i className="fa-solid fa-plus"></i>
                <span>Add account</span>
              </button>
            )}
          </div>
        </div>
        {user?.role === "admin" && (
          <div className="mt-5 flex justify-end items-center gap-3">
            <Dropdown
              menu={{
                items: usersOptions,
                onClick: ({ key }: { key: string }) =>
                  handleMenuClick(key, "user"),
              }}
              trigger={["click"]}
              className="hover:cursor-pointer flex items-center h-[40px] rounded-lg-buttons"
            >
              <a
                className={`border rounded-md px-3 py-2 ${
                  filters.user ? "bg-yellow-100" : ""
                } `}
                onClick={(e) => e.preventDefault()}
              >
                <Space>
                  User <span className="capitalize">{userLabel}</span>
                  {filters.user ? (
                    <span onClick={(e: any) => removeFilterValues(e, "user")}>
                      <i className="fas fa-times"></i>
                    </span>
                  ) : (
                    <ArrowDropDownIcon />
                  )}
                </Space>
              </a>
            </Dropdown>
          </div>
        )}
      </div>
      <div className="px-5 w-full h-full">
        <AccountsTable
          isLoading={accountsStatus === "loading"}
          data={filteredData}
          userRole={user?.role}
        />
      </div>
      <StickyPagination pagination={pagination} setPagination={setPagination} />
    </>
  );
}

export default Accounts;
