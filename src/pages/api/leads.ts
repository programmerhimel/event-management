import { NextApiRequest, NextApiResponse } from "next";
import dbConnect from "../../../lib/db";
import LeadModel, { Lead } from "../../../models/Lead";
import { getSession } from "next-auth/react";
import { Types } from "mongoose";

dbConnect();

/**
 * @swagger
 * components:
 *   schemas:
 *     Lead:
 *       type: object
 *       properties:
 *         avatar:
 *           type: string
 *           description: The avatar URL of the lead
 *         name:
 *           type: string
 *           description: The name of the lead
 *         role:
 *           type: string
 *           description: The role of the lead
 *         url:
 *           type: string
 *           description: The URL associated with the lead
 *         country:
 *           type: string
 *           description: The country of the lead
 *         category:
 *           type: string
 *           description: The category of the lead
 *         company:
 *           type: string
 *           description: The company of the lead
 *         location:
 *           type: string
 *           description: The location of the lead
 *         industry:
 *           type: string
 *           description: The industry of the lead
 *         note:
 *           type: string
 *           description: Additional notes about the lead
 *         tags:
 *           type: array
 *           items:
 *             type: string
 *           description: Tags associated with the lead
 *         resource:
 *           type: string
 *           description: The resource from which the lead was obtained
 *         meta_data:
 *           type: object
 *           additionalProperties: true
 *           description: Metadata associated with the lead
 *         assigned_user:
 *           type: string
 *           description: The user to whom the lead is assigned
 *         assigned_account:
 *           type: string
 *           description: The account to which the lead is assigned
 *         action_status:
 *           type: string
 *           description: The action status of the lead
 *         accept_status:
 *           type: string
 *           description: The acceptance status of the lead
 */

/**
 * @swagger
 * /api/leads:
 *   get:
 *     summary: Gets a list of leads
 *     tags: [Leads]
 *     responses:
 *       200:
 *         description: The list of leads
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref: '#/components/schemas/Lead'
 *       500:
 *         description: Error fetching leads
 *   post:
 *     summary: Creates a new lead
 *     tags: [Leads]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/Lead'
 *     responses:
 *       201:
 *         description: The created lead
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Lead'
 *       500:
 *         description: Error creating lead
 * /api/leads/{id}:
 *   put:
 *     summary: Updates an existing lead
 *     tags: [Leads]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: The lead id
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/Lead'
 *     responses:
 *       200:
 *         description: The updated lead
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Lead'
 *       404:
 *         description: Lead not found
 *       500:
 *         description: Error updating lead
 *   delete:
 *     summary: Deletes an existing lead
 *     tags: [Leads]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: The lead id
 *     responses:
 *       200:
 *         description: Lead deleted successfully
 *       404:
 *         description: Lead not found
 *       500:
 *         description: Error deleting lead
 */

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const { method } = req;

  switch (method) {
    case "GET":
      const session = await getSession({ req });
      let query = {} as Record<string, string | any>;
      if (session?.user.role === "user") {
        query.assigned_user = session.user.id;
      } else if (session?.user.role !== "admin") {
        res.status(403).json({ error: "Authentication failed" });
      }
      for (const key in req.query) {
        if (req.query.hasOwnProperty(key) && req.query[key]) {
          if (["page", "limit"].includes(key)) continue;
          if (key === "assigned_user" && !query.assigned_user) {
            query[key] = new Types.ObjectId(req.query[key] as string);
          } else if (key === "search") {
            const regex = new RegExp(req.query[key] as string, "i");
            query = {
              ...query,
              $or: [
                { name: regex },
                { country: regex },
                { role: regex },
                { company: regex },
                { location: regex },
                { industry: regex },
                { url: regex },
                { category: regex },
                { note: regex },
                { tags: regex },
                { resource: regex },
              ],
            };
          } else {
            query[key] = req.query[key];
          }
        }
      }
      const DEFAULT_LIMIT = 10;
      const page = req.query.page ? Number(req.query.page) : 1;
      const limit = req.query.limit ? Number(req.query.limit) : DEFAULT_LIMIT;

      if (req.query.assigned_user === "") {
        try {
          const leads = await LeadModel.find({ assigned_user: null })
            .skip((Number(page) - 1) * limit)
            .limit(limit);

          const count = await LeadModel.countDocuments({ assigned_user: null });
          const startIndex = (page - 1) * limit + 1;
          const endIndex = Math.min(page * limit, count);
          let totalRecords = 0;
          if (session?.user.role === "admin") {
            totalRecords = await LeadModel.countDocuments({
              assigned_user: null,
            });
          } else if (session?.user.role === "user") {
            totalRecords = await LeadModel.countDocuments({
              assigned_user: session.user.id,
            });
          }

          res.status(200).json({
            leads,
            count,
            startIndex,
            endIndex,
            totalRecords,
            currentPage: page,
          });
        } catch (error) {
          console.error("Error fetching leads:", error);
          res.status(500).json({ error: "Error fetching leads" });
        }
      } else {
        try {
          const leads = await LeadModel.find({
            ...query,
            assigned_user: { $ne: null },
          })
            .skip((Number(page) - 1) * limit)
            .limit(limit)
            .populate("assigned_user", "name _id");

          const count = await LeadModel.countDocuments({
            ...query,
            assigned_user: { $ne: null },
          });
          const startIndex = (page - 1) * limit + 1;
          const endIndex = Math.min(page * limit, count);
          let totalRecords = 0;
          if (session?.user.role === "admin") {
            totalRecords = await LeadModel.countDocuments({
              assigned_user: { $ne: null },
            });
          } else if (session?.user.role === "user") {
            totalRecords = await LeadModel.countDocuments({
              assigned_user: session.user.id,
            });
          }

          res.status(200).json({
            leads,
            count,
            startIndex,
            endIndex,
            totalRecords,
            currentPage: page,
          });
        } catch (error) {
          console.error("Error fetching leads:", error);
          res.status(500).json({ error: "Error fetching leads" });
        }
      }
      break;

    case "POST":
      try {
        const leadData: Lead = req.body;
        if (!leadData.assigned_user) {
          delete leadData.assigned_user;
          delete leadData.assigned_account;
        }
        if (!leadData.action_status) {
          delete leadData.action_status;
        }
        if (!leadData.accept_status) {
          delete leadData.accept_status;
        }
        const newLead = await LeadModel.create(leadData);
        res.status(201).json(newLead);
      } catch (error) {
        console.error("Error creating lead:", error);
        res.status(500).json({ error: "Error creating lead" });
      }
      break;

    case "PUT":
      const USER_ALLOWED_FIELDS = [
        "note",
        "tags",
        "action_status",
        "accept_status",
      ];
      try {
        let leadData: Partial<Lead> = req.body;
        const session = await getSession({ req });
        if (session?.user.role === "user") {
          const payload = {} as Partial<Lead>;
          for (const key in leadData) {
            if (USER_ALLOWED_FIELDS.includes(key)) {
              payload[key as keyof Lead] = req.body[key];
            }
          }
          leadData = payload;
        }
        const { id } = req.query;
        const updatedLead = await LeadModel.findByIdAndUpdate(id, leadData, {
          new: true,
          runValidators: true,
        });
        if (!updatedLead) {
          res.status(404).json({ error: "Lead not found" });
        } else {
          res.status(200).json(updatedLead);
        }
      } catch (error) {
        console.error("Error updating lead:", error);
        res.status(500).json({ error: "Error updating lead" });
      }
      break;

    case "DELETE":
      try {
        const { id } = req.query;
        const deletedLead = await LeadModel.findByIdAndDelete(id);
        if (!deletedLead) {
          res.status(404).json({ error: "Lead not found" });
        } else {
          res.status(200).json({ message: "Lead deleted successfully" });
        }
      } catch (error) {
        console.error("Error deleting lead:", error);
        res.status(500).json({ error: "Error deleting lead" });
      }
      break;

    default:
      res.setHeader("Allow", ["GET", "POST", "PUT", "DELETE"]);
      res.status(405).end(`Method ${method} Not Allowed`);
      break;
  }
}
