import { NextApiRequest, NextApiResponse } from "next";
import dbConnect from "../../../lib/db";
import AccountModel, { Account } from "../../../models/Account";
import { getSession } from "next-auth/react";

dbConnect();

/**
 * @swagger
 * components:
 *   schemas:
 *     Account:
 *       type: object
 *       required:
 *         - name
 *         - email
 *       properties:
 *         id:
 *           type: string
 *           description: The auto-generated id of the account
 *         name:
 *           type: string
 *           description: The name of the account holder
 *         email:
 *           type: string
 *           description: The email of the account holder
 */

/**
 * @swagger
 * /api/accounts:
 *   get:
 *     summary: Gets a list of user accounts
 *     tags: [Accounts]
 *     responses:
 *       200:
 *         description: The list of accounts
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref: '#/components/schemas/Account'
 *       500:
 *         description: Error fetching user accounts
 */

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  if (req.method !== "GET") {
    return res.status(405).json({ error: "Method not allowed" });
  }

  try {
    const { userId } = req.query;
    const session = await getSession({ req });

    let query = {} as Record<string, string | any>;

    if (session?.user.role === "user") {
      query.user = session.user.id;
    } else if (userId) {
      query.user = userId;
    }

    const accounts = await AccountModel.find(query).populate(
      'user',
      'name _id'
    );

    res.status(200).json(accounts);
  } catch (error) {
    console.error("Error fetching accounts:", error);
    res.status(500).json({ error: "Error fetching accounts" });
  }
}
