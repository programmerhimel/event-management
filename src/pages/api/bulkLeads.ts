import { NextApiRequest, NextApiResponse } from 'next';
import dbConnect from '../../../lib/db';
import LeadModel, { Lead } from '../../../models/Lead';

dbConnect();

/**
 * @swagger
 * components:
 *   schemas:
 *     Lead:
 *       type: object
 *       properties:
 *         avatar:
 *           type: string
 *           description: The avatar URL of the lead
 *         name:
 *           type: string
 *           description: The name of the lead
 *         role:
 *           type: string
 *           description: The role of the lead
 *         url:
 *           type: string
 *           description: The URL associated with the lead
 *         country:
 *           type: string
 *           description: The country of the lead
 *         category:
 *           type: string
 *           description: The category of the lead
 *         company:
 *           type: string
 *           description: The company of the lead
 *         location:
 *           type: string
 *           description: The location of the lead
 *         industry:
 *           type: string
 *           description: The industry of the lead
 *         note:
 *           type: string
 *           description: Additional notes about the lead
 *         tags:
 *           type: array
 *           items:
 *             type: string
 *           description: Tags associated with the lead
 *         resource:
 *           type: string
 *           description: The resource from which the lead was obtained
 *         meta_data:
 *           type: object
 *           additionalProperties: true
 *           description: Metadata associated with the lead
 *         assigned_user:
 *           type: string
 *           description: The user to whom the lead is assigned
 *         assigned_account:
 *           type: string
 *           description: The account to which the lead is assigned
 *         action_status:
 *           type: string
 *           description: The action status of the lead
 *         accept_status:
 *           type: string
 *           description: The acceptance status of the lead
 */

export default async function handler(req: NextApiRequest, res: NextApiResponse) {
  if (req.method === 'POST') {
    try {
      const leads: Partial<Lead>[] = req.body;
      const urls = leads.map(lead => lead.url).filter(Boolean) as string[];

      const existingLeads = await LeadModel.find({ url: { $in: urls } }).select('url');
      const existingUrls = new Set(existingLeads.map(lead => lead.url));

      const newLeads = leads.filter(lead => lead.url && !existingUrls.has(lead.url!));

      const transformedLeads = newLeads.map(lead => ({
        avatar: lead.avatar || "",
        name: lead.name,
        role: lead.role || "",
        url: lead.url || "",
        country: lead.country || "",
        category: lead.category || "",
        company: lead.company || "",
        location: lead.location || "",
        industry: lead.industry || "",
        note: lead.note || "",
        tags: lead.tags || [],
        resource: lead.resource || "",
        meta_data: lead.meta_data || {},
        assigned_user: lead.assigned_user || null,
        assigned_account: lead.assigned_account || null,
        action_status: lead.action_status || "open",
        accept_status: lead.accept_status || "open",
      }));

      const createdLeads = await LeadModel.insertMany(transformedLeads);
      res.status(200).json({ 
        message: `${createdLeads.length} leads created successfully`, 
        added: createdLeads.length, 
        duplicates: leads.length - createdLeads.length 
      });
    } catch (error) {
      console.error('Error saving leads:', error);
      res.status(500).json({ error: error || 'Error saving leads to database' });
    }
  } else {
    res.setHeader('Allow', ['POST']);
    res.status(405).end(`Method ${req.method} Not Allowed`);
  }
}
