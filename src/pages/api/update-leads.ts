import { NextApiRequest, NextApiResponse } from "next";
import dbConnect from "../../../lib/db";
import LeadModel, { Lead } from "../../../models/Lead";
import { getSession } from "next-auth/react";
import { ObjectId } from "mongodb";

dbConnect();

/**
 * @swagger
 * components:
 *   schemas:
 *     Lead:
 *       type: object
 *       properties:
 *         avatar:
 *           type: string
 *           description: The avatar URL of the lead
 *         name:
 *           type: string
 *           description: The name of the lead
 *         role:
 *           type: string
 *           description: The role of the lead
 *         url:
 *           type: string
 *           description: The URL associated with the lead
 *         country:
 *           type: string
 *           description: The country of the lead
 *         category:
 *           type: string
 *           description: The category of the lead
 *         company:
 *           type: string
 *           description: The company of the lead
 *         location:
 *           type: string
 *           description: The location of the lead
 *         industry:
 *           type: string
 *           description: The industry of the lead
 *         note:
 *           type: string
 *           description: Additional notes about the lead
 *         tags:
 *           type: array
 *           items:
 *             type: string
 *           description: Tags associated with the lead
 *         resource:
 *           type: string
 *           description: The resource from which the lead was obtained
 *         meta_data:
 *           type: object
 *           additionalProperties: true
 *           description: Metadata associated with the lead
 *         assigned_user:
 *           type: string
 *           description: The user to whom the lead is assigned
 *         assigned_account:
 *           type: string
 *           description: The account to which the lead is assigned
 *         action_status:
 *           type: string
 *           description: The action status of the lead
 *         accept_status:
 *           type: string
 *           description: The acceptance status of the lead
 */

/**
 * @swagger
 * /api/leads:
 *   get:
 *     summary: Gets a list of leads
 *     tags: [Leads]
 *     responses:
 *       200:
 *         description: The list of leads
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref: '#/components/schemas/Lead'
 *       500:
 *         description: Error fetching leads
 *   post:
 *     summary: Creates a new lead
 *     tags: [Leads]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/Lead'
 *     responses:
 *       201:
 *         description: The created lead
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Lead'
 *       500:
 *         description: Error creating lead
 * /api/leads/{id}:
 *   put:
 *     summary: Updates an existing lead
 *     tags: [Leads]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: The lead id
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/Lead'
 *     responses:
 *       200:
 *         description: The updated lead
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Lead'
 *       404:
 *         description: Lead not found
 *       500:
 *         description: Error updating lead
 *   delete:
 *     summary: Deletes an existing lead
 *     tags: [Leads]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: The lead id
 *     responses:
 *       200:
 *         description: Lead deleted successfully
 *       404:
 *         description: Lead not found
 *       500:
 *         description: Error deleting lead
 */

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  if (req.method !== 'PUT') {
    return res.status(405).json({ error: 'Method not allowed' });
  }
  try {
    const { ids, ...payload } = req.body;
    if (!ids || !Array.isArray(ids) || !Object.keys(payload).length) {
      return res.status(400).json({ error: 'Invalid payload' });
    }
    const objectIdArray = ids.map(id => new ObjectId(id));
    const updateResult = await LeadModel.updateMany(
      { _id: { $in: objectIdArray } },
      { ...payload }
    );
    console.log(updateResult, 'update result')
    res.status(200).json({
      message: 'Leads updated successfully',
      modifiedCount: updateResult.modifiedCount,
    });
  } catch (error) {
    console.error('Error updating leads:', error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
}
