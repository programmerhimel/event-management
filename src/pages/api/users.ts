import { NextApiRequest, NextApiResponse } from "next";
import bcrypt from "bcrypt";
import dbConnect from "../../../lib/db";
import UserModel, { User } from "../../../models/User";

dbConnect();

/**
 * @swagger
 * components:
 *   schemas:
 *     User:
 *       type: object
 *       properties:
 *         name:
 *           type: string
 *           description: The name of the user
 *         email:
 *           type: string
 *           description: The email of the user
 *         password:
 *           type: string
 *           description: The password of the user
 */

/**
 * @swagger
 * /api/users:
 *   get:
 *     summary: Gets a list of users
 *     tags: [Users]
 *     responses:
 *       200:
 *         description: The list of users
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref: '#/components/schemas/User'
 *       500:
 *         description: Error fetching users
 *   post:
 *     summary: Creates a new user
 *     tags: [Users]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/User'
 *     responses:
 *       201:
 *         description: The created user
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/User'
 *       500:
 *         description: Error creating user
 * /api/users/{id}:
 *   put:
 *     summary: Updates an existing user
 *     tags: [Users]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: The user id
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/User'
 *     responses:
 *       200:
 *         description: The updated user
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/User'
 *       404:
 *         description: User not found
 *       500:
 *         description: Error updating user
 *   delete:
 *     summary: Deletes an existing user
 *     tags: [Users]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: The user id
 *     responses:
 *       200:
 *         description: User deleted successfully
 *       404:
 *         description: User not found
 *       500:
 *         description: Error deleting user
 */

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const { method } = req;

  switch (method) {
    case "GET":
      try {
        const { id } = req.query;
        let query = {} as Record<string, string | any>
        const DEFAULT_LIMIT = 10
        if (id) {
          const user = await UserModel.findById(id);
          if (!user) {
            res.status(404).json({ error: "User not found" });
          } else {
            res.status(200).json(user);
          }
        } else {
          const page = (req.query.page) ? Number(req.query.page) : 1
          const limit = (req.query.limit) ? Number(req.query.limit) : DEFAULT_LIMIT
          const users = await UserModel.find({})
          .skip((Number(page) - 1) * limit)
          .limit(limit)
          .select('name email role _id');
          const count = await UserModel.countDocuments(query);

          res.status(200).json({users, count, currentPage: page});
        }
      } catch (error) {
        console.error("Error fetching users:", error);
        res.status(500).json({ error: "Error fetching users" });
      }
      break;

    case "POST":
      try {
        const userData: User = req.body;
        const newUser = await UserModel.create(userData);
        res.status(201).json(newUser);
      } catch (error) {
        console.error("Error creating user:", error);
        res.status(500).json({ error: "Error creating user" });
      }
      break;

    case "PUT":
      try {
        const { id } = req.query;
        const userData: Partial<User> = req.body;

        if (userData.password) {
          const salt = await bcrypt.genSalt(10);
          userData.password = await bcrypt.hash(userData.password, salt);
        }

        const updatedUser = await UserModel.findByIdAndUpdate(id, userData, {
          new: true,
          runValidators: true,
        });

        if (!updatedUser) {
          res.status(404).json({ error: "User not found" });
        } else {
          res.status(200).json(updatedUser);
        }
      } catch (error) {
        console.error("Error updating user:", error);
        res.status(500).json({ error: "Error updating user" });
      }
      break;

    case "DELETE":
      try {
        const { id } = req.query;
        const deletedUser = await UserModel.findByIdAndDelete(id);
        if (!deletedUser) {
          res.status(404).json({ error: "User not found" });
        } else {
          res.status(200).json({ message: "User deleted successfully" });
        }
      } catch (error) {
        console.error("Error deleting user:", error);
        res.status(500).json({ error: "Error deleting user" });
      }
      break;

    default:
      res.setHeader("Allow", ["GET", "POST"]);
      res.status(405).end(`Method ${method} Not Allowed`);
      break;
  }
}
