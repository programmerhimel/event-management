import { NextApiRequest, NextApiResponse } from "next";
import dbConnect from "../../../lib/db";
import UserModel, { User } from "../../../models/User";

dbConnect();

const verifyTokenAndCheckRole = (token: string): boolean => {
  const validToken = process.env.ADMIN_TOKEN;

  if (token === validToken) {
    return true;
  }

  return false;
};

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const { method, body } = req;

  switch (method) {
    case "POST":
      try {
        const token = body.token;

        if (!token || !verifyTokenAndCheckRole(token)) {
          return res.status(401).json({ error: "Unauthorized" });
        }

        const role = "admin";
        const userData: User = { ...body, role };

        const newUser = await UserModel.create(userData);
        res.status(201).json(newUser);
      } catch (error) {
        console.error("Error creating user:", error);
        res.status(500).json({ error: "Error creating user" });
      }
      break;

    default:
      res.setHeader("Allow", ["POST"]);
      res.status(405).end(`Method ${method} Not Allowed`);
      break;
  }
}
