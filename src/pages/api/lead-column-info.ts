import { NextApiRequest, NextApiResponse } from "next";
import dbConnect from "../../../lib/db";
import UserLeadColumnInfoModel from "../../../models/LeadColumnInfo";

dbConnect();

/**
 * @swagger
 * components:
 *   schemas:
 *     LeadColumnInfo:
 *       type: object
 *       properties:
 *         userId:
 *           type: string
 *           description: The user ID
 *         leadColumnInfo:
 *           type: array
 *           items:
 *             type: object
 *             properties:
 *               title:
 *                 type: string
 *               checked:
 *                 type: boolean
 */

/**
 * @swagger
 * /api/lead-column-info:
 *   get:
 *     summary: Gets lead column info
 *     tags: [LeadColumnInfo]
 *     parameters:
 *       - in: query
 *         name: userId
 *         schema:
 *           type: string
 *         description: The user ID
 *       - in: query
 *         name: id
 *         schema:
 *           type: string
 *         description: The lead column info ID
 *     responses:
 *       200:
 *         description: The lead column info
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref: '#/components/schemas/LeadColumnInfo'
 *       404:
 *         description: Lead column info not found
 *       500:
 *         description: Error fetching lead column info
 *   post:
 *     summary: Creates or updates lead column info
 *     tags: [LeadColumnInfo]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/LeadColumnInfo'
 *     responses:
 *       200:
 *         description: Lead column info updated successfully
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/LeadColumnInfo'
 *       201:
 *         description: Lead column info created successfully
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/LeadColumnInfo'
 *       500:
 *         description: Error creating or updating lead column info
 * /api/lead-column-info/{id}:
 *   put:
 *     summary: Updates lead column info by ID
 *     tags: [LeadColumnInfo]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: The lead column info ID
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/LeadColumnInfo'
 *     responses:
 *       200:
 *         description: Lead column info updated successfully
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/LeadColumnInfo'
 *       404:
 *         description: Lead column info not found
 *       500:
 *         description: Error updating lead column info
 *   delete:
 *     summary: Deletes lead column info by ID
 *     tags: [LeadColumnInfo]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: The lead column info ID
 *     responses:
 *       200:
 *         description: Lead column info deleted successfully
 *       404:
 *         description: Lead column info not found
 *       500:
 *         description: Error deleting lead column info
 */

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const { method } = req;

  switch (method) {
    case "GET":
      try {
        const { userId, id } = req.query;

        console.log(userId);

        if (id) {
          // Fetch a single lead column info by its ID
          const leadColumnInfo = await UserLeadColumnInfoModel.findById(id);
          if (!leadColumnInfo) {
            res.status(404).json({ error: "Lead column info not found" });
          } else {
            res.status(200).json(leadColumnInfo);
          }
        } else if (userId) {
          // Fetch lead column info for a specific user
          const leadColumnInfos = await UserLeadColumnInfoModel.find({
            userId,
          });
          res.status(200).json(leadColumnInfos);
        } else {
          // Fetch all lead column infos
          const leadColumnInfos = await UserLeadColumnInfoModel.find();
          res.status(200).json(leadColumnInfos);
        }
      } catch (error) {
        console.error("Error fetching lead column info:", error);
        res.status(500).json({ error: "Error fetching lead column info" });
      }
      break;

    case "POST":
      try {
        const { userId, leadColumnInfo } = req.body;

        // Check if data for the user already exists
        const existingLeadColumnInfo = await UserLeadColumnInfoModel.findOne({
          userId,
        });

        if (existingLeadColumnInfo) {
          // Update the existing data
          existingLeadColumnInfo.leadColumnInfo = leadColumnInfo;
          await existingLeadColumnInfo.save();
          res.status(200).json(existingLeadColumnInfo);
        } else {
          // Create new data
          const newLeadColumnInfo = new UserLeadColumnInfoModel({
            userId,
            leadColumnInfo,
          });
          await newLeadColumnInfo.save();
          res.status(201).json(newLeadColumnInfo);
        }
      } catch (error) {
        console.error("Error creating or updating lead column info:", error);
        res
          .status(500)
          .json({ error: "Error creating or updating lead column info" });
      }
      break;

    case "PUT":
      try {
        const { id } = req.query;
        const { leadColumnInfo } = req.body;

        const updatedLeadColumnInfo =
          await UserLeadColumnInfoModel.findByIdAndUpdate(
            id,
            { leadColumnInfo },
            { new: true, runValidators: true }
          );

        if (!updatedLeadColumnInfo) {
          res.status(404).json({ error: "Lead column info not found" });
        } else {
          res.status(200).json(updatedLeadColumnInfo);
        }
      } catch (error) {
        console.error("Error updating lead column info:", error);
        res.status(500).json({ error: "Error updating lead column info" });
      }
      break;

    case "DELETE":
      try {
        const { id } = req.query;
        const deletedLeadColumnInfo =
          await UserLeadColumnInfoModel.findByIdAndDelete(id);
        if (!deletedLeadColumnInfo) {
          res.status(404).json({ error: "Lead column info not found" });
        } else {
          res
            .status(200)
            .json({ message: "Lead column info deleted successfully" });
        }
      } catch (error) {
        console.error("Error deleting lead column info:", error);
        res.status(500).json({ error: "Error deleting lead column info" });
      }
      break;

    default:
      res.setHeader("Allow", ["GET", "POST", "PUT", "DELETE"]);
      res.status(405).end(`Method ${method} Not Allowed`);
      break;
  }
}
