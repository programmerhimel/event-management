import { NextApiRequest, NextApiResponse } from "next";
import dbConnect from "../../../lib/db";
import UserModel from "../../../models/User";
import { getSession } from "next-auth/react";
import LeadModel from "../../../models/Lead";
import { Types } from "mongoose";

dbConnect();

const MODULES = {
  country: "country",
  category: "category",
  action_status: "action_status",
  user: "user",
  all: "all",
};

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const { method } = req;

  switch (method) {
    case "GET":
      try {
        const { module } = req.query;
        const session = await getSession({ req });
        let query = {} as Record<string, string | any>;
        if (session?.user.role === "user") {
          query.assigned_user = new Types.ObjectId(session.user.id);
        } else if (session?.user.role !== "admin") {
          res.status(403).json({ error: "Authentication failed" });
          return;
        }

        const pipeline:any = [];
        if (query.assigned_user) {
          pipeline.push({
            $match: {
              assigned_user: query.assigned_user
            },
          });
        }

        const fetchCountries = async () => {
          const countryPipeline = [
            ...pipeline,
            { $group: { _id: "$country" } },
            { $project: { _id: 0, country: "$_id" } },
          ];
          const uniqueCountry = await LeadModel.aggregate(countryPipeline);
          return uniqueCountry.map((doc) => doc.country).filter((item) => item);
        };

        const fetchCategories = async () => {
          const categoryPipeline = [
            ...pipeline,
            { $group: { _id: "$category" } },
            { $project: { _id: 0, category: "$_id" } },
          ];
          const uniqueCategories = await LeadModel.aggregate(categoryPipeline);
          return uniqueCategories.map((doc) => doc.category).filter((item) => item);
        };

        const fetchUsers = async () => {
          if (query.assigned_user) {
            const user = await UserModel.findById(query.assigned_user).select('_id name');
            return [user];
          } else {
            return await UserModel.find().select('_id name');
          }
        };

        switch (module) {
          case MODULES.country:
            try {
              const countries = await fetchCountries();
              res.status(200).json(countries);
            } catch (error) {
              res.status(500).json({ error: "Error fetching data" });
            }
            break;
          case MODULES.category:
            try {
              const categories = await fetchCategories();
              res.status(200).json(categories);
            } catch (error) {
              res.status(500).json({ error: "Error fetching data" });
            }
            break;
          case MODULES.action_status:
            try {
              pipeline.push({
                $group: {
                  _id: "$action_status",
                },
              });
              pipeline.push({
                $project: {
                  _id: 0,
                  action_status: "$_id",
                },
              });
              const actionStatus = await LeadModel.aggregate(pipeline);
              const status = actionStatus.map((doc) => doc.action_status).filter((item) => item);
              res.status(200).json(status);
            } catch (error) {
              res.status(500).json({ error: "Error fetching data" });
            }
            break;
          case MODULES.user:
            try {
              const users = await fetchUsers();
              res.status(200).json(users);
            } catch (error) {
              res.status(500).json({ error: "Error fetching data" });
            }
            break;
          case MODULES.all:
            try {
              const [countries, categories, users] = await Promise.all([
                fetchCountries(),
                fetchCategories(),
                fetchUsers(),
              ]);
              res.status(200).json({ countries, categories, users });
            } catch (error) {
              res.status(500).json({ error: "Error fetching data" });
            }
            break;
          default:
            res.status(200).json([]);
        }
      } catch (error) {
        console.error("Error fetching users:", error);
        res.status(500).json({ error: "Error fetching users" });
      }
      break;

    default:
      res.setHeader("Allow", ["GET"]);
      res.status(405).end(`Method ${method} Not Allowed`);
      break;
  }
}
