import { NextApiRequest, NextApiResponse } from "next";
import dbConnect from "../../../lib/db";
import AccountModel, { Account } from "../../../models/Account";
import { getSession } from "next-auth/react";

dbConnect();


/**
 * @swagger
 * components:
 *   schemas:
 *     Account:
 *       type: object
 *       required:
 *         - name
 *         - email
 *       properties:
 *         id:
 *           type: string
 *           description: The auto-generated id of the account
 *         name:
 *           type: string
 *           description: The name of the account holder
 *         email:
 *           type: string
 *           description: The email of the account holder
 */

/**
 * @swagger
 * /api/accounts:
 *   get:
 *     summary: Gets a list of accounts
 *     tags: [Accounts]
 *     responses:
 *       200:
 *         description: The list of accounts
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref: '#/components/schemas/Account'
 *       500:
 *         description: Error fetching accounts
 *   post:
 *     summary: Creates a new account
 *     tags: [Accounts]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/Account'
 *     responses:
 *       201:
 *         description: The created account
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Account'
 *       500:
 *         description: Error creating account
 * /api/accounts/{id}:
 *   put:
 *     summary: Updates an existing account
 *     tags: [Accounts]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: The account id
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/Account'
 *     responses:
 *       200:
 *         description: The updated account
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Account'
 *       404:
 *         description: Account not found
 *       500:
 *         description: Error updating account
 *   delete:
 *     summary: Deletes an existing account
 *     tags: [Accounts]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: The account id
 *     responses:
 *       200:
 *         description: Account deleted successfully
 *       404:
 *         description: Account not found
 *       500:
 *         description: Error deleting account
 */

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const { method } = req;

  switch (method) {
    case "GET":
      try {
        const { userId } = req.query;
        const session = await getSession({ req });
        let query = {} as Record<string, string | any>
        const DEFAULT_LIMIT = 10
        const page = (req.query.page) ? Number(req.query.page) : 1
        const limit = (req.query.limit) ? Number(req.query.limit) : DEFAULT_LIMIT

        if (session?.user.role === 'user') {
          query.user = session.user.id
        } else if (userId) {
          query.user = userId
        }
        const accounts = await AccountModel.find(query)
        .skip((Number(page) - 1) * limit)
        .limit(limit)
        .populate('user', 'name _id');
        const count = await AccountModel.countDocuments(query);
        res.status(200).json({accounts, count, currentPage: page});
      } catch (error) {
        console.error("Error fetching accounts:", error);
        res.status(500).json({ error: "Error fetching accounts" });
      }
      break;

    case "POST":
      try {
        const accountData: Account = req.body;
        const newAccount = await AccountModel.create(accountData);
        res.status(201).json(newAccount);
      } catch (error) {
        console.error("Error creating account:", error);
        res.status(500).json({ error: "Error creating account" });
      }
      break;

    case "PUT":
      const USER_ALLOWED_FIELDS = ['status']
      try {

        const { id } = req.query;
        let accountData: Partial<Account> = req.body;
        const session = await getSession({ req });
        if (session?.user.role === 'user') {
          const payload = {} as Partial<Account>
          for (const key in accountData) {
            if (USER_ALLOWED_FIELDS.includes(key)) {
              payload[key as keyof Account] = req.body[key]
            }
          }
          accountData = payload
        }
        const updatedAccount = await AccountModel.findByIdAndUpdate(
          id,
          accountData,
          {
            new: true,
            runValidators: true,
          }
        );

        if (!updatedAccount) {
          res.status(404).json({ error: "Account not found" });
        } else {
          res.status(200).json(updatedAccount);
        }
      } catch (error) {
        console.error("Error updating account:", error);
        res.status(500).json({ error: "Error updating account" });
      }
      break;

    case "DELETE":
      try {
        const { id } = req.query;
        const deletedAccount = await AccountModel.findByIdAndDelete(id);
        if (!deletedAccount) {
          res.status(404).json({ error: "Account not found" });
        } else {
          res.status(200).json({ message: "Account deleted successfully" });
        }
      } catch (error) {
        console.error("Error deleting account:", error);
        res.status(500).json({ error: "Error deleting account" });
      }
      break;

    default:
      res.setHeader("Allow", ["GET", "POST", "PUT", "DELETE"]);
      res.status(405).end(`Method ${method} Not Allowed`);
      break;
  }
}
