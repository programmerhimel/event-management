import Layout from "@/components/Layout";
import { ClientContext } from "@/context/ClientContext";
import "@/styles/globals.css";
import "@/styles/swagger-ui.css";
import { SessionProvider } from "next-auth/react";
import type { AppProps } from "next/app";
import { useRouter } from "next/router";
import { useState, useEffect } from "react";
import { Provider } from "react-redux";
import { reduxWrapper } from "../redux/store";
import { fetchColumnInfo, fetchLeads } from "@/redux/actions/leadsActions";
import { fetchUsers } from "@/redux/actions/usersActions";
import { fetchAccounts } from "@/redux/actions/accountsActions";

export default function App({ Component, pageProps }: AppProps) {
  const { store } = reduxWrapper.useWrappedStore(pageProps);
  const [isEditMode, setIsEditMode] = useState(false);
  const [editAccountData, setEditAccountData] = useState<Account>();
  const [editUserData, setEditUserData] = useState<User>();
  const columnsArray = [
    { title: "avatar", checked: true },
    { title: "name", checked: true },
    { title: "role", checked: true },
    { title: "url", checked: true },
    { title: "country", checked: true },
    { title: "category", checked: true },
    { title: "company", checked: true },
    { title: "location", checked: true },
    { title: "industry", checked: true },
    { title: "note", checked: true },
    { title: "tags", checked: true },
    { title: "resource", checked: true },
    { title: "action_status", checked: true },
    { title: "accept_status", checked: true },
    { title: "createdAt", checked: true },
    { title: "updatedAt", checked: true },
  ];
  const [storedLeadColumns, setStoredLeadColumns] =
    useState<LeadColumn[]>(columnsArray);

  const router = useRouter();
  const path = router.pathname;

  useEffect(() => {
    const localLeadColumnsData = localStorage.getItem("leadColumns");
    if (localLeadColumnsData) {
      const parsed = JSON.parse(localLeadColumnsData);
      setStoredLeadColumns(parsed);
    }
    store.dispatch(fetchColumnInfo());
    store.dispatch(fetchUsers());
    store.dispatch(fetchAccounts());
  }, []);

  const contextValues = {
    isEditMode,
    setIsEditMode,
    editAccountData,
    setEditAccountData,
    editUserData,
    setEditUserData,
    storedLeadColumns,
    setStoredLeadColumns,
    columnsArray,
  };
  return (
    <Provider store={store}>
      <ClientContext.Provider value={contextValues}>
        <SessionProvider session={pageProps.session}>
          {path === "/signin" ||
          path === "/register" ||
          path === "/swagger/docs" ? (
            <Component {...pageProps} />
          ) : (
            <Layout>
              <Component {...pageProps} />
            </Layout>
          )}
        </SessionProvider>
      </ClientContext.Provider>
    </Provider>
  );
}
