// types/next-auth.d.ts
import NextAuth from "next-auth";

declare module "next-auth" {
  interface Session {
    accessToken?: string;
    user: {
      id: string,
      name: string;
      email: string;
      role: string;
    };
  }

  

  interface JWT {
    accessToken?: string;
  }
}
