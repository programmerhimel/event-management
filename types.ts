interface Lead {
  _id: string;
  avatar: string;
  name: string;
  role: string;
  url: string;
  country: string;
  category: string;
  company: string;
  location: string;
  industry: string;
  note: string;
  tags: string[];
  resource: string;
  meta_data: Record<string, any>;
  assigned_user?: { _id: string; name: string };
  assigned_account: { _id: string; name: string };
  action_status: string;
  accept_status: string;
  createdAt: string;
  updatedAt: string;
}

interface Account {
  _id: string;
  name: string;
  user?: { _id: string; name: string };
  status: "active" | "pending" | "restricted" | "archived" | "assigned";
  createdAt: string;
  updatedAt: string;
}

interface User {
  _id: string;
  name: string;
  email: string;
  password: string;
  role: "user" | "admin";
  avatar: string;
  createdAt: string;
  updatedAt: string;
}

interface LeadColumn {
  title: string;
  checked: boolean;
}
